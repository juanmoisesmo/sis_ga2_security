﻿
alter PROCEDURE [dbo].[USP_Del_Vehiculos_IMD]
(
		 @Id_Vehiculos_IMD int
		,@Id_Diseno int
		,@Id_Repet_Equivalentes int
)
AS
begin
begin tran  

	DELETE [dbo].[Vehiculos_IMD] 
	WHERE [Id_Diseno] = @Id_Diseno
	  --AND  [Id_Vehiculos_IMD] = @Id_Vehiculos_IMD
	  --AND [Id_Repet_Equivalentes] = @Id_Repet_Equivalentes

commit tran
end
go

alter PROCEDURE [dbo].[USP_ListaProyDiseno_Lst]  
@Num_Proyecto varchar(50),
@Fecha_Proyecto decimal(10),
@Fecha_Contrato decimal(10),
@Id_Usuario   int,
@Id_Distrito  int,
@Id_Provincia int,
@Id_Departamento int,
@Id_TipoDiseno int = null

AS
BEGIN
DECLARE @QUERY AS VARCHAR(5000)
DECLARE @INTQUERY AS INT
SET @QUERY = 'SELECT PR.Id_Proyecto
		
		,Num_Proyecto 
		,Proyecto
		,PR.Fecha_Proyecto 					  
		,U.Dni 
		,U.Apellido + ''' + ' ' +  '''+  U.Nombre as Ingeniero
		,CA.Cargo
		,PR.Fecha_Contrato
		,p.[Id_Diseno] 
		,ISNULL(p.Num_Diseno,''' + ''') AS Num_Diseno'+'
		,ISNULL(td.Tipo_Diseno,''' + ''') AS Tipo_Diseno'+' 
		,ISNULL(TR.Tramo	,''' + ''') AS Tramo'+'  			  
		,ISNULL(DEP.Departamento  + ''/'  + ''' + PRO.Provincia + ''/'  + 		
		''' + D.Distrito,''' + ''') AS Ubigeo		
		FROM ASOCEMPROD.dbo.Proyecto PR
		left join dbo.Diseno_Pavimento P
		ON pr.Id_Proyecto=p.Id_Proyecto
		left join Distrito D ON
		D.Id_Distrito=p.Id_Distrito
		left join PROVINCIA PRO ON
		D.Id_Provincia=PRO.Id_Provincia
		LEFT JOIN Departamento DEP ON
		PRO.Id_Departamento=DEP.Id_Departamento
		LEFT JOIN Usuario U ON
		PR.Id_Usuario=U.Id_Usuario
		LEFT JOIN Cargo CA ON
		U.Id_Cargo=CA.Id_Cargo
		left join Tipo_Diseno td on
		p.Id_TipoDiseno=td.Id_TipoDiseno
		left join dbo.Tramo TR
		ON p.Id_Tramo=TR.Id_Tramo
		WHERE PR.Estado = 1 '


SET @INTQUERY=(SELECT   CHARINDEX('WHERE', @QUERY) )
	
	IF (@INTQUERY>0) AND (@Num_Proyecto <> '')
		BEGIN
			SET @QUERY = @QUERY + ' AND PR.[Num_Proyecto] = '+ '''' + @Num_Proyecto + ''''
		END
	ELSE IF (@INTQUERY=0) AND (@Num_Proyecto <> '')
		BEGIN
			SET @QUERY = @QUERY + ' WHERE PR.[Num_Proyecto] = ' + @Num_Proyecto
		END

SET @INTQUERY=(SELECT   CHARINDEX('WHERE', @QUERY) )

	IF (@INTQUERY>0) AND (@Fecha_Proyecto > 0)
		BEGIN
			SET @QUERY = @QUERY + ' AND PR.[Fecha_Proyecto] = ' + + CAST(@Fecha_Proyecto AS varchar) 
		END
	ELSE IF (@INTQUERY=0) AND (@Fecha_Proyecto > 0)
		BEGIN
			SET @QUERY = @QUERY + ' WHERE PR.[Fecha_Proyecto] = ' + + CAST(@Fecha_Proyecto AS varchar) 
		END

SET @INTQUERY=(SELECT   CHARINDEX('WHERE', @QUERY) )

	IF (@INTQUERY>0)AND(@Fecha_Contrato > 0)
		BEGIN
			SET @QUERY = @QUERY + ' AND PR.[Fecha_Contrato] = ' + + CAST(@Fecha_Contrato AS varchar) 
		END
	ELSE IF (@INTQUERY=0) AND (@Fecha_Contrato > 0)
		BEGIN
			SET @QUERY = @QUERY + ' WHERE PR.[Fecha_Contrato] = ' + + CAST(@Fecha_Contrato AS varchar) 
		END	

SET @INTQUERY=(SELECT   CHARINDEX('WHERE', @QUERY) )
	IF (@INTQUERY>0)AND(@Id_Usuario > 0)
		BEGIN
			SET @QUERY = @QUERY + ' AND PR.[Id_Usuario] = ' + + CAST(@Id_Usuario AS varchar) 
		END
	ELSE IF (@INTQUERY=0) AND (@Id_Usuario > 0)
		BEGIN
			SET @QUERY = @QUERY + ' WHERE PR.[Id_Usuario] = ' + + CAST(@Id_Usuario AS varchar) 
		END	


/*ULTIMO CAMBIO - TIPODISEÑO td.Id_TipoDiseno*/
if @Id_TipoDiseno is not null
begin
SET @INTQUERY=(SELECT   CHARINDEX('WHERE', @QUERY) )
	IF (@INTQUERY>0)AND(@Id_TipoDiseno > 0)
		BEGIN
			SET @QUERY = @QUERY + ' AND TD.[Id_TipoDiseno] = ' + + CAST(@Id_TipoDiseno AS varchar) 
		END
	ELSE IF (@INTQUERY=0) AND (@Id_TipoDiseno > 0)
		BEGIN
			SET @QUERY = @QUERY + ' WHERE TD.[Id_TipoDiseno] = ' + + CAST(@Id_TipoDiseno AS varchar) 
		END	
end
		
/*ULTIMO CAMBIO - TIPODISEÑO td.Id_TipoDiseno*/

SET @INTQUERY=(SELECT   CHARINDEX('WHERE', @QUERY) )

IF (@INTQUERY>0) AND (@Id_Distrito > 0)
		BEGIN
			SET @QUERY = @QUERY + ' AND P.Id_Distrito = ' + + CAST(@Id_Distrito AS varchar) 
		END
ELSE IF (@INTQUERY=0) AND (@Id_Distrito > 0)
		BEGIN
			SET @QUERY = @QUERY + ' WHERE P.Id_Distrito = ' + + CAST(@Id_Distrito AS varchar) 
		END

SET @INTQUERY=(SELECT   CHARINDEX('WHERE', @QUERY) )
IF (@INTQUERY>0) AND (@Id_Provincia > 0)
		BEGIN
			SET @QUERY = @QUERY + ' AND PRO.Id_Provincia = ' + + CAST(@Id_Provincia AS varchar) 
		END
ELSE IF (@INTQUERY=0) AND (@Id_Provincia > 0)
		BEGIN
			SET @QUERY = @QUERY + ' WHERE PRO.Id_Provincia = ' + + CAST(@Id_Provincia AS varchar) 
		END

SET @INTQUERY=(SELECT   CHARINDEX('WHERE', @QUERY) )
IF (@INTQUERY>0) AND (@Id_Departamento > 0)
		BEGIN
			SET @QUERY = @QUERY + ' AND DEP.Id_Departamento = ' + + CAST(@Id_Departamento AS varchar) 
		END
ELSE IF (@INTQUERY=0) AND (@Id_Departamento > 0)
		BEGIN
			SET @QUERY = @QUERY + ' WHERE DEP.Id_Departamento = ' + + CAST(@Id_Departamento AS varchar) 
		END


SET @QUERY = @QUERY  + ' order by PR.[Id_Proyecto] asc';

 --  PRINT(@QUERY)
 EXECUTE (@QUERY)
 /* WHERE [Num_Proyecto]='001'  
  AND [Fecha_Proyecto]='20190205'
  AND [Fecha_Contrato]='20190103'
  AND PR.Id_Usuario=2
  AND P.Id_Distrito=3
  AND PRO.Id_Provincia=1
  AND DEP.Id_Departamento=1;*/

END
go

CREATE PROCEDURE [dbo].[USP_InsUpd_Proyecto]
(	  
	  @IdProyecto  INT,
	  @IdDiseno  INT,	  
      @Num_Proyecto varchar(50),
      @Proyecto varchar(50),
      @Fecha_Proyecto decimal (8,0),
      @Estado int,
      @Id_Usuario int,
      @Fecha_Creacion decimal (8,0),
      @Fecha_Contrato decimal (8,0),
      @Hora_Creacion decimal (12,0),
      @Usr_Creacion varchar(50),
      @Fecha_Actualizacion decimal (8,0),
      @Hora_Actualizacion  decimal (12,0),
      @Usr_Actualizacion varchar(50),
	  @Num_Diseno varchar(50),
	  @Id_Tramo int,
	  @Id_Reglamento int,
	  @Id_TipoDiseno int,
	  @Id_Distrito int,
	  @IdProyecto_out  INT OUTPUT ,
	  @IdDiseno_out  INT OUTPUT
)
AS
BEGIN
	BEGIN TRY

	BEGIN TRANSACTION 
	
		IF @IdDiseno = 0
		BEGIN
			INSERT INTO [dbo].[Proyecto]
					   ([Num_Proyecto]
					   ,[Proyecto]
					   ,[Fecha_Proyecto]
					   ,[Estado]
					   ,[Id_Usuario]
					   ,[Fecha_Creacion]
					   ,[Fecha_Contrato]
					   ,[Hora_Creacion]
					   ,[Usr_Creacion]
					   ,[Fecha_Actualizacion]
					   ,[Hora_Actualizacion]
					   ,[Usr_Actualizacion])
				 VALUES
						 (@Num_Proyecto,
						  @Proyecto ,
						  @Fecha_Proyecto ,
						  @Estado ,
						  @Id_Usuario ,
						  @Fecha_Creacion ,
						  @Fecha_Contrato ,
						  @Hora_Creacion ,
						  @Usr_Creacion ,
						  @Fecha_Actualizacion ,
						  @Hora_Actualizacion  ,
						  @Usr_Actualizacion )

			SELECT @IdProyecto = @@IDENTITY 
	
			INSERT INTO [dbo].[Diseno_Pavimento]
					   ([Id_Proyecto]
					   ,[Num_Diseno]
					   ,[Estado]
					   ,[Id_Tramo]
					   ,[Id_Reglamento]
					   ,[Id_TipoDiseno]
					   ,[Id_Distrito]
					   ,[Fecha_Creacion]
					   ,[Hora_Creacion]
					   ,[Usr_Creacion]
					   ,[Fecha_Actualizacion]
					   ,[Hora_Actualizacion]
					   ,[Usr_Actualizacion])
				 VALUES
					   (@IdProyecto,
						@Num_Diseno,
						@Estado,
						@Id_Tramo,
						@Id_Reglamento,
						@Id_TipoDiseno,
						@Id_Distrito,
						@Fecha_Creacion ,			
						@Hora_Creacion ,
						@Usr_Creacion ,
						@Fecha_Actualizacion ,
						@Hora_Actualizacion  ,
						@Usr_Actualizacion )
	
			COMMIT TRANSACTION

			SELECT @IdDiseno = @@IDENTITY
		END
		ELSE 
		BEGIN
			
			 UPDATE [dbo].[Diseno_Pavimento]
				SET [Num_Diseno] = @Num_Diseno,
					[Estado] = @Estado,
					[Id_Tramo] = @Id_Tramo,
					[Id_Reglamento] = @Id_Reglamento,
					[Id_TipoDiseno] = @Id_TipoDiseno,
					[Id_Distrito] = @Id_Distrito,
					[Fecha_Actualizacion] = @Fecha_Actualizacion,
					[Hora_Actualizacion] = @Hora_Actualizacion,
					[Usr_Actualizacion] = @Usr_Actualizacion
			  WHERE [Id_Diseno] = @IdDiseno

			 
			 UPDATE [dbo].[Proyecto]
			   SET [Num_Proyecto] = @Num_Proyecto,
				   [Proyecto] = @Proyecto,
				   [Fecha_Proyecto] = @Fecha_Proyecto,
				   [Estado] =  @Estado ,
				   [Id_Usuario] =  @Id_Usuario ,
				   [Fecha_Contrato] = @Fecha_Contrato,
				   [Fecha_Actualizacion] = @Fecha_Actualizacion,
				   [Hora_Actualizacion] =@Hora_Actualizacion,
				   [Usr_Actualizacion] = @Usr_Actualizacion
			 WHERE  [Id_Proyecto] = @IdProyecto
			
			 SELECT @IdProyecto_out = @IdProyecto
			 SELECT @IdDiseno_out = @IdDiseno

			COMMIT TRANSACTION
		END

	END TRY
	BEGIN CATCH

		SELECT
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_STATE() AS ErrorState,
			ERROR_SEVERITY() AS ErrorSeverity,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage;
			ROLLBACK TRANSACTION
	
	END CATCH;

end
go


CREATE TABLE CoeficientesProyecto
(
	Id_Diseno int,
	Est_Capas_A1_Rec decimal(16,2),
	Est_Capas_A1_Ing decimal(16,2),
	Est_Capas_A2_Mos decimal(16,2),
	Est_Capas_A2_Rec decimal(16,2),
	Est_Capas_A2_Ing decimal(16,2),
	Est_Capas_A3_Mos decimal(16,2),
	Est_Capas_A3_Rec decimal(16,2),
	Est_Capas_A3_Ing decimal(16,2),
	Dren_Capas_M2_Calidad_Valor int,
	Dren_Capas_M2_Porc_Sat decimal(16,2),
	Dren_Capas_M2_Coef_Dren decimal(16,2),
	Dren_Capas_M2_Coef_Dren_Ing decimal(16,2),
	Dren_Capas_M3_Calidad_Valor int,
	Dren_Capas_M3_Porc_Sat decimal(16,2),
	Dren_Capas_M3_Coef_Dren decimal(16,2),
	Dren_Capas_M3_Coef_Dren_Ing decimal(16,2),
	SN_Propuesto decimal(16,2),
	SN_Requerido decimal(16,2),
	Resultado_Capa_Asf decimal(16,2),
	Resultado_Base decimal(16,2),
	Resultado_SubBase decimal(16,2),
	Usr_Creacion varchar(100),
	Fecha_Creacion Datetime,
	Usr_Actualizacion varchar(100),
	Fecha_Actualizacion Datetime,
)
GO

alter procedure Usp_InsUpd_CoeficientesProyecto
(
	@Id_Diseno int,
	@Est_Capas_A1_Rec decimal(16,2),
	@Est_Capas_A1_Ing decimal(16,2),
	@Est_Capas_A2_Mos decimal(16,2),
	@Est_Capas_A2_Rec decimal(16,2),
	@Est_Capas_A2_Ing decimal(16,2),
	@Est_Capas_A3_Mos decimal(16,2),
	@Est_Capas_A3_Rec decimal(16,2),
	@Est_Capas_A3_Ing decimal(16,2),
	@Dren_Capas_M2_Calidad_Valor int,
	@Dren_Capas_M2_Porc_Sat decimal(16,2),
	@Dren_Capas_M2_Coef_Dren decimal(16,2),
	@Dren_Capas_M2_Coef_Dren_Ing decimal(16,2),
	@Dren_Capas_M3_Calidad_Valor int,
	@Dren_Capas_M3_Porc_Sat decimal(16,2),
	@Dren_Capas_M3_Coef_Dren decimal(16,2),
	@Dren_Capas_M3_Coef_Dren_Ing decimal(16,2),
	@SN_Propuesto decimal(16,2),
	@SN_Requerido decimal(16,2),
	@Resultado_Capa_Asf decimal(16,2),
	@Resultado_Base decimal(16,2),
	@Resultado_SubBase decimal(16,2),
	@UsuarioCreUpd varchar(100)
)
as
begin
	
	declare @exist int = 0
	set @exist = (select 1 from CoeficientesProyecto where Id_Diseno = @Id_Diseno)

	if @exist = 0
	begin
		
		Insert into CoeficientesProyecto (	Id_Diseno ,
											Est_Capas_A1_Rec ,
											Est_Capas_A1_Ing ,
											Est_Capas_A2_Mos ,
											Est_Capas_A2_Rec ,
											Est_Capas_A2_Ing ,
											Est_Capas_A3_Mos ,
											Est_Capas_A3_Rec ,
											Est_Capas_A3_Ing ,
											Dren_Capas_M2_Calidad_Valor ,
											Dren_Capas_M2_Porc_Sat ,
											Dren_Capas_M2_Coef_Dren ,
											Dren_Capas_M2_Coef_Dren_Ing ,
											Dren_Capas_M3_Calidad_Valor ,
											Dren_Capas_M3_Porc_Sat ,
											Dren_Capas_M3_Coef_Dren ,
											Dren_Capas_M3_Coef_Dren_Ing ,
											SN_Propuesto ,
											SN_Requerido ,
											Resultado_Capa_Asf ,
											Resultado_Base ,
											Resultado_SubBase,
											Usr_Creacion,
											Fecha_Creacion) 
							values (@Id_Diseno ,
									@Est_Capas_A1_Rec ,
									@Est_Capas_A1_Ing ,
									@Est_Capas_A2_Mos ,
									@Est_Capas_A2_Rec ,
									@Est_Capas_A2_Ing ,
									@Est_Capas_A3_Mos ,
									@Est_Capas_A3_Rec ,
									@Est_Capas_A3_Ing ,
									@Dren_Capas_M2_Calidad_Valor ,
									@Dren_Capas_M2_Porc_Sat ,
									@Dren_Capas_M2_Coef_Dren ,
									@Dren_Capas_M2_Coef_Dren_Ing ,
									@Dren_Capas_M3_Calidad_Valor ,
									@Dren_Capas_M3_Porc_Sat ,
									@Dren_Capas_M3_Coef_Dren ,
									@Dren_Capas_M3_Coef_Dren_Ing ,
									@SN_Propuesto ,
									@SN_Requerido ,
									@Resultado_Capa_Asf ,
									@Resultado_Base ,
									@Resultado_SubBase,
									@UsuarioCreUpd,
									getdate());

	end
	else 
	begin
		
		Update CoeficientesProyecto 
		set Est_Capas_A1_Rec = @Est_Capas_A1_Rec,
			Est_Capas_A1_Ing = @Est_Capas_A1_Ing,
			Est_Capas_A2_Mos = @Est_Capas_A2_Mos,
			Est_Capas_A2_Rec = @Est_Capas_A2_Rec,
			Est_Capas_A2_Ing = @Est_Capas_A2_Ing,
			Est_Capas_A3_Mos = @Est_Capas_A3_Mos,
			Est_Capas_A3_Rec = @Est_Capas_A3_Mos,
			Est_Capas_A3_Ing = @Est_Capas_A3_Mos,
			Dren_Capas_M2_Calidad_Valor = @Dren_Capas_M2_Calidad_Valor,
			Dren_Capas_M2_Porc_Sat = @Dren_Capas_M2_Porc_Sat,
			Dren_Capas_M2_Coef_Dren = @Dren_Capas_M2_Coef_Dren,
			Dren_Capas_M2_Coef_Dren_Ing = @Dren_Capas_M2_Coef_Dren_Ing,
			Dren_Capas_M3_Calidad_Valor = @Dren_Capas_M3_Calidad_Valor,
			Dren_Capas_M3_Porc_Sat = @Dren_Capas_M3_Porc_Sat,
			Dren_Capas_M3_Coef_Dren = @Dren_Capas_M3_Coef_Dren,
			Dren_Capas_M3_Coef_Dren_Ing = @Dren_Capas_M3_Coef_Dren_Ing,
			SN_Propuesto = @SN_Propuesto,
			SN_Requerido = @SN_Requerido,
			Resultado_Capa_Asf = @Resultado_Capa_Asf,
			Resultado_Base = @Resultado_Base,
			Resultado_SubBase = @Resultado_SubBase,
			Usr_Actualizacion = @UsuarioCreUpd,
			Fecha_Actualizacion = GETDATE()
		Where Id_Diseno = @Id_Diseno
	end

end
go

---
CREATE TABLE ProyectoDiseno
(
	Id_Diseno bigint primary key identity(1,1),
	Json_Data nvarchar(max),
	Tipo_Diseno int,
	Estado  bit,
	Usr_Creacion varchar(100),
	Fecha_Creacion datetime,
	Usr_Actualizacion varchar(100),
	Fecha_Actualizacion datetime
)
go

CREATE TABLE [dbo].[Matriz_Tasa_Crecimiento1_Asfalto](
	[Id_Tasa_Crecimiento1] [int] IDENTITY(1,1) NOT NULL,
	[Nro_Anio] [int] NULL,
	[Id_Diseno] [int] NULL,
	[Id_Tipo_Vehiculo] [int] NULL,
	[Valor] [decimal](18, 2) NULL
)
GO

ALTER PROCEDURE usp_grabar_proyecto_diseno
(
	@p_Id_Diseno bigint,
	@p_Json_Data nvarchar(max),
	@p_Tipo_Diseno int,
	@p_Usuario varchar(100),
	@p_Id_Diseno_out bigint out
)
AS
BEGIN

	declare @exist int = 0

	set @exist = (select COUNT(1) from ProyectoDiseno where Id_Diseno = @p_Id_Diseno)

	if @exist = 0
	begin
		
		insert into ProyectoDiseno (Json_Data,
									Tipo_Diseno,
									Estado,
									Usr_Creacion,
									Fecha_Creacion)
						values (    @p_Json_Data,
									@p_Tipo_Diseno,
									1,									
									@p_Usuario,
									getdate())

		select @p_Id_Diseno_out = @@IDENTITY

	end
	else
	begin
		 
		update ProyectoDiseno 
		set Json_Data = @p_Json_Data,
			Usr_Actualizacion = @p_Usuario,
			Fecha_Actualizacion = GETDATE()			
		where Id_Diseno = @p_Id_Diseno

		select @p_Id_Diseno_out = @p_Id_Diseno
	end

END
GO


CREATE PROCEDURE usp_listar_todos_proyecto_diseno
(
	@p_Tipo_Diseno int = null,
	@p_Usuario varchar(100)
)
AS
BEGIN
	
	Select 	Id_Diseno,
			Json_Data,
			Tipo_Diseno,
			Usr_Creacion,
			Fecha_Creacion,
			Usr_Actualizacion,
			Fecha_Actualizacion
	From ProyectoDiseno
	Where (Tipo_Diseno = @p_Tipo_Diseno or @p_Tipo_Diseno is null)
	  and ((Usr_Creacion = @p_Usuario or Usr_Actualizacion = @p_Usuario) or @p_Usuario is null)
	  and Estado = 1

END
GO

CREATE PROCEDURE usp_obtener_proyecto_diseno
(
	@p_Id_Diseno bigint
)
AS
BEGIN
	
	Select 	Id_Diseno,
			Json_Data,
			Tipo_Diseno,
			Usr_Creacion,
			Fecha_Creacion,
			Usr_Actualizacion,
			Fecha_Actualizacion
	From ProyectoDiseno
	Where Id_Diseno = @p_Id_Diseno

END
GO

CREATE PROCEDURE usp_actualiza_estado_proyecto_diseno
(
	@p_Id_Diseno bigint,
	@p_Estado bit,
	@p_Usuario varchar(100)
)
AS
BEGIN
	
	update ProyectoDiseno
	set Estado = @p_Estado,
		Usr_Actualizacion = @p_Usuario 
	where Id_Diseno = @p_Id_Diseno

END
GO


CREATE PROCEDURE [dbo].[USP_Ins_Tasa_Crecimi1_Asfalto]
(	  
			@Id_Diseno int
		   ,@Nro_Anio int 
           ,@Id_Tipo_Vehiculo int 
           ,@Valor decimal(10,2)
)		  
as
BEGIN

	INSERT INTO [dbo].[Matriz_Tasa_Crecimiento1_Asfalto]
           ([Id_Diseno]
		   ,Nro_Anio
           ,[Id_Tipo_Vehiculo]
           ,[Valor])
     VALUES
           (@Id_Diseno
		   ,@Nro_Anio
           ,@Id_Tipo_Vehiculo
           ,@Valor)

END
GO

CREATE PROCEDURE [dbo].[USP_del_TasasCrecimiento1_Asfalto]
	@Id_Diseno int
AS
BEGIN

	DELETE FROM [Matriz_Tasa_Crecimiento1_Asfalto] WHERE [Id_Diseno]= @Id_Diseno
	
END
GO

ALTER TABLE [Ejes] ADD Posicion INT
go

ALTER PROCEDURE [dbo].[USP_Sel_Eje_x_Vehi]
	@idVehiculo int

AS
BEGIN
	
	DECLARE @QUERY AS VARCHAR(5000)



	SET @QUERY = 'SELECT  [Id_Eje]				
				  ,[Valor]	
				  ,[Posicion]
			  FROM [dbo].[Ejes] '
				  

	IF (@idVehiculo > 0)
		BEGIN
			SET @QUERY = @QUERY + ' where Id_Vehiculo = ' + CAST(@idVehiculo AS varchar) 
		END
	else
	SET @QUERY = @QUERY + '  order by [Id_Eje] asc';

	--PRINT(@QUERY)
	EXECUTE (@QUERY)
END
go

--31122021
--USP_USUARIO_LISTAR_TODO NULL,NULL,NULL,NULL,NULL

ALTER PROCEDURE USP_USUARIO_ELIMINAR
(
	@P_ID_USUARIO INT,
	@P_ID_USUARIO_UPDATE VARCHAR(10),
	@P_ESTADO VARCHAR(10)
)
AS
BEGIN
	
	UPDATE Usuario
	SET Estado = @P_ESTADO,
		Usr_Actualizacion = @P_ID_USUARIO_UPDATE
	WHERE Id_Usuario = @P_ID_USUARIO

END
GO

ALTER PROCEDURE USP_USUARIO_LISTAR_TODO 
(
	 @p_Dni varchar(10) = null,
	 @p_Apellido  varchar(50) = null,
	 @p_Nombre varchar(50) = null,
	 @p_Id_Cargo int = null,
	 @p_estado int = null
)
AS
BEGIN
	SELECT Id_Usuario,
		  Dni,
		  Apellido,
		  Nombre,
		  Direccion,
		  Telefono,
		  Correo,
		  Cip,
		  C.Cargo,
		  U.Id_Cargo,
		  EstadoValor = Estado,
		  Estado_Descripcion = (CASE WHEN Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END),
		  Usuario,
		  Clave
	FROM Usuario U 
	  INNER JOIN Cargo C ON C.Id_Cargo = U.Id_Cargo
	WHERE  U.Id_Cargo <> 4 
	 AND (Dni = @p_Dni OR @p_Dni IS NULL)
	 AND (Apellido = @p_Apellido OR @p_Apellido IS NULL)
	 AND (Nombre = @p_Nombre OR @p_Nombre IS NULL)
	 AND (U.Id_Cargo = @p_Id_Cargo OR @p_Id_Cargo IS NULL)
	 AND (Estado = @p_estado OR @p_estado IS NULL)

END
GO

ALTER PROCEDURE USP_USUARIO_INS_UPD
(
	@P_Id_Usuario VARCHAR(10),
	@P_Dni VARCHAR(10),
	@P_Apellido VARCHAR(50),
	@P_Nombre VARCHAR(50),
	@P_Direccion VARCHAR(100),
	@P_Telefono VARCHAR(50),
	@P_Correo VARCHAR(50),
	@P_Usuario VARCHAR(50),
	@P_Clave VARCHAR(50),
	@P_Estado INT,
	@P_Id_Cargo INT,
	@P_Id_UsuarioInsUpd VARCHAR(10)
)
AS
BEGIN
	
	DECLARE @EXISTE INT
	SET @EXISTE = (SELECT COUNT(1) FROM Usuario WHERE Id_Usuario = @P_Id_Usuario)

	IF @EXISTE = 0
	BEGIN
		INSERT INTO Usuario (Dni,Apellido,Nombre,Direccion,Telefono,Correo,Usuario,Clave,Estado,Id_Cargo,Usr_Creacion)
		VALUES (@P_Dni,@P_Apellido,@P_Nombre,@P_Direccion,@P_Telefono,@P_Correo,@P_Usuario,@P_Clave,@P_Estado,@P_Id_Cargo,@P_Id_UsuarioInsUpd);
	END
	ELSE
	BEGIN
		
		UPDATE Usuario
		SET Dni = @P_Dni,
			Apellido = @P_Apellido,
			Nombre = @P_Nombre,
			Direccion = @P_Direccion,
			Telefono = @P_Telefono,
			Correo = @P_Correo,
			Clave = @P_Clave,
			Usr_Actualizacion = @P_Id_UsuarioInsUpd
		WHERE Id_Usuario = @P_Id_Usuario;

	END


END
GO