﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLTramo
    {

        public List<BETramo> ListarTramoxId(int idTramo)
        {
            DACTramo objDAO = new DACTramo();
            return objDAO.ListarTramoxId(idTramo);
        }

        public List<BETramo> ListarTipoDisenos()
        {
            DACTramo objDAO = new DACTramo();
            return objDAO.ListarTramos();
        }
    }
}
