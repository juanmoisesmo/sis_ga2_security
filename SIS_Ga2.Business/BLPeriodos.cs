﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLPeriodos
    {

        public List<BEPeriodo> ListarPeriodos(int idPeriodo)
        {
            DACPeriodo objDAO = new DACPeriodo();
            return objDAO.ListarPeriodos(idPeriodo);
        }


    }
}
