﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLCoefPresion
    {

        public List<BECoefPresion> ListaCoefPresion()
        {
            DACCoefPresion objDAO = new DACCoefPresion();
            return objDAO.ListaCoefPresion();
        }

    }
}
