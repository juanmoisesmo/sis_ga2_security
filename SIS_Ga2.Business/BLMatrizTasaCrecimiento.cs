﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;

namespace SIS_Ga2.Business
{
    public class BLMatrizTasaCrecimiento
    {
        public List<BEMatrizTasaCrecimiento> ListarTasaCrec1(int Id_Diseno)
        {
            DACMatrizTasaCrecimiento objDAO = new DACMatrizTasaCrecimiento();
            return objDAO.ListarTasaCrec1(Id_Diseno);
        }
        public List<BEMatrizTasaCrecimiento> ListarTasaCrec2(int Id_Diseno)
        {
            DACMatrizTasaCrecimiento objDAO = new DACMatrizTasaCrecimiento();
            return objDAO.ListarTasaCrec2(Id_Diseno);
        }
        public int GuardartasaCrecimiento(BEMatrizTasaCrecimiento objEntidad, int NroMatriz)
        {
            DACMatrizTasaCrecimiento objDAO = new DACMatrizTasaCrecimiento();
            if (NroMatriz == 1)
            {

                return objDAO.GuardartasaCrecimiento1(objEntidad);
            }

            else
            {
                return objDAO.GuardartasaCrecimiento2(objEntidad);
            }
        }

        public int GuardartasaCrecimiento1Asfalto(BEMatrizTasaCrecimiento objEntidad)
        {
            DACMatrizTasaCrecimiento objDAO = new DACMatrizTasaCrecimiento();
            return objDAO.GuardartasaCrecimiento1Asfalto(objEntidad);

        }

        public int DelTasaCrecimiento(int Id_Diseno)
        {
            DACMatrizTasaCrecimiento objDAO = new DACMatrizTasaCrecimiento();
            return objDAO.DeletetasaCrecimiento1(Id_Diseno);
        }

        public int DeletetasaCrecimiento1Asfalto(int IdDiseno)
        {
            DACMatrizTasaCrecimiento objDAO = new DACMatrizTasaCrecimiento();
            return objDAO.DeletetasaCrecimiento1Asfalto(IdDiseno);
        }
    }
}
