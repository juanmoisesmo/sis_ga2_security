﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;

namespace SIS_Ga2.Business
{
    public class BLMatrizEE
    {
        public List<BEMatrizEE> ListarInfoMatrizEE(int idParametro)
        {
            DACMatrizEE objDAO = new DACMatrizEE();
            return objDAO.ListarInfoMatrizEE(idParametro);
        }
    }
}
