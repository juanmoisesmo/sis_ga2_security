﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLReglamento
    {


        public List<BEReglamentos> ListarReglamentosxId(int idReglamento)
        {
            DACReglamentos objDAO = new DACReglamentos();
            return objDAO.ListarReglamentosxId(idReglamento);
        }

        public List<BEReglamentos> ListarReglamentos()
        {
            DACReglamentos objDAO = new DACReglamentos();
            return objDAO.ListarReglamentos();
        }
    }
}
