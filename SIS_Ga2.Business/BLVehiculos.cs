﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLVehiculos
    {

        public List<BEVehiculos> ListarVehiculos(int Id_Tipo_Vehiculo)
        {
            DACVehiculos objDAO = new DACVehiculos();
            return objDAO.ListarVehiculos(Id_Tipo_Vehiculo);
        }


    }
}
