﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;



namespace SIS_Ga2.Business
{
    public class BLParametrosDiseno
    {
        public int Guardar(BEParametroDiseno objEntidad)
        {
            DACParametrosDiseno objDAO = new DACParametrosDiseno();
            return objDAO.Registrar(objEntidad);
        }

        public int Actualizar(BEParametroDiseno objEntidad)
        {
            DACParametrosDiseno objDAO = new DACParametrosDiseno();
            return objDAO.Actualizar(objEntidad);
        }
    }
}
