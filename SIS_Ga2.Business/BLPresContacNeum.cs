﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;

namespace SIS_Ga2.Business
{
    public class BLPresContacNeum
    {
        public List<BEPresContacNeum> ListarValorFP(int Id_Espesor, int Id_Presion)
        {
            DACPresContacNeum objDAO = new DACPresContacNeum();
            return objDAO.ListarValorFP(Id_Espesor, Id_Presion);
        }
    }
}

