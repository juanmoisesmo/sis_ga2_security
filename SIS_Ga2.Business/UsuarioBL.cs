﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;

namespace SIS_Ga2.Business
{
    public class UsuarioBL
    {
        UsuarioDAO objdao = new UsuarioDAO();

        public List<BEUsuario> ListarUsuario(BEUsuario obj)
        {
            return objdao.ListarUsuario(obj);
        }

        public List<rol> ListarRolesPorSociedad(int idsociedad, int idusuario)
        {
            return objdao.ListarRolesPorSociedad(idsociedad, idusuario);
        }
        public bool AgregarRol(int idusuario, int idsociedad, int idrol)
        {
            return objdao.AgregarRol(idusuario, idsociedad, idrol);
        }
        public bool DeleteRolUsuario(int idusuario, int idsociedad)
        {
            return objdao.DeleteRolUsuario(idusuario, idsociedad);
        }



    }
}
