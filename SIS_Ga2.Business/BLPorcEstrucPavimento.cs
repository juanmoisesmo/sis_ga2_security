﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLPorcEstrucPavimento
    {

        public List<BEPorcEstructPavimento> ListarPorcEstrucPavimento(int Id_Porc_Estruc_Pavimen)
        {
            DACPorcEstrucPavimento objDAO = new DACPorcEstrucPavimento();
            return objDAO.ListarPorcEstrucPavimento(Id_Porc_Estruc_Pavimen);
        }


    }
}
