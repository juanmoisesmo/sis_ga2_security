﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;

namespace SIS_Ga2.Business
{
    public class BLProyectoDiseno
    {
        private readonly DACProyectoDiseno _datos;

        public BLProyectoDiseno()
        {
            _datos = new DACProyectoDiseno();
        }

        public long GuardarProyectoDiseno(string jsonData, string usuario, int tipodiseno, long iddiseno)
        {
            try
            {
                return _datos.GuardarProyectoDiseno(jsonData, usuario, tipodiseno, iddiseno);
            }
            catch
            {
                throw;
            }

        }

        public List<BEProyectoDiseno> ListarTodoProyectoDiseno(string usuario, int? tipo_diseno = null)
        {
            try
            {
                return _datos.ListarTodoProyectoDiseno(usuario, tipo_diseno);
            }
            catch
            {
                throw;
            }
        }

        public BEProyectoDiseno ObtenerProyectoDiseno(int iddiseno)
        {
            try
            {
                return _datos.ObtenerProyectoDiseno(iddiseno);
            }
            catch
            {

                throw;
            }
        }

        public void ActualizaEstadoProyectoDiseno(bool estado, string usuario, int iddiseno)
        {
            try
            {
                _datos.ActualizaEstadoProyectoDiseno(estado, usuario, iddiseno);
            }
            catch
            {
                throw;
            }
        }
    }
}
