﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLTipoDiseno
    {

        public List<BETipoDiseno> ListarTipoDisenoxId(int idReglamento)
        {
            DACTipoDiseno objDAO = new DACTipoDiseno();
            return objDAO.ListarTipoDisenoxId(idReglamento);
        }

        public List<BETipoDiseno> ListarTipoDisenos()
        {
            DACTipoDiseno objDAO = new DACTipoDiseno();
            return objDAO.ListarTipoDisenos();
        }
    }
}
