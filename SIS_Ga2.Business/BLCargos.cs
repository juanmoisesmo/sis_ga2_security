﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLCargos
    {

        public List<BECargo> ListarCargoXId(int idCargo)
        {
            DACCargo objDAO = new DACCargo();
            return objDAO.ListarCargosxId(idCargo);
        }

        public List<BECargo> ListarCargos()
        {
            DACCargo objDAO = new DACCargo();
            return objDAO.ListarCargos();
        }
    }
}
