﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLTiempoElimAgua
    {

        public List<BETiempoElimAgua> ListarTiempoElimAguaXID(int idTiempoELimAgua)
        {
            DACTiempoElimAgua objDAO = new DACTiempoElimAgua();
            return objDAO.ListarTiempoElimAguaXID(idTiempoELimAgua);
        }


    }
}
