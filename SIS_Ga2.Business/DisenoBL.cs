﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;

namespace SIS_Ga2.Business
{
    public class DisenoBL
    {
        public List<ParametrosDiseno> ListarParametrosxTipoDiseno(int idTipoDiseno)
        {
            try
            {
                DisenoDAO objDAO = new DisenoDAO();
                return objDAO.ListarParametrosxTipoDiseno(idTipoDiseno);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
