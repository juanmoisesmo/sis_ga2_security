﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLProyectos
    {

        public List<BEProyecto> ListarProyectosxId(int idProyecto)
        {
            DACProyectos objDAO = new DACProyectos();
            return objDAO.ListarProyectosxId(idProyecto);
        }

        public List<BEProyecto> ListarProyectos()
        {
            DACProyectos objDAO = new DACProyectos();
            return objDAO.ListarProyectos();
        }

        public int GuardarCoeficientes(BECoefEstructura objEntidad)
        {
            DACProyectos objDAO = new DACProyectos();
            return objDAO.GuardarCoeficientes(objEntidad);
        }

        public void GrabarCoeficientes(BECoefEstructura objEntidad)
        {
            DACProyectos objDAO = new DACProyectos();
            objDAO.GrabarCoeficientes(objEntidad);
        }
    }
}


