﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLCoefEspesor
    {

        public List<BECoefEspesor> ListaCoefEspesor()
        {
            DACCoefEspesor objDAO = new DACCoefEspesor();
            return objDAO.ListarEspesores();
        }

    }
}
