﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class CatalogoBL
    {

        public List<Catalogo> ListarPorTipo(string Tipo)
        {
            CatalogoDAO objDAO = new CatalogoDAO();
            return objDAO.ListarPorTipo(Tipo);
        }

    }
}
