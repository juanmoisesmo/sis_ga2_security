﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLDisenos
    {

        public List<BEDiseno> ListarDisenoxId(int idDiseno)
        {
            DACDiseno objDAO = new DACDiseno();
            return objDAO.ListarDisenoxId(idDiseno);
        }

        public List<BEDiseno> ListarDisenos()
        {
            DACDiseno objDAO = new DACDiseno();
            return objDAO.ListarDisenos();
        }
    }
}
