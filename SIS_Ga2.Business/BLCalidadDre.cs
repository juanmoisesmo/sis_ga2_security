﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLCalidadDre
    {

        public List<BECalidadDre> ListarCaliDrenajesXID(int idCargo)
        {
            DACCalidadDre objDAO = new DACCalidadDre();
            return objDAO.ListarCaliDrenajesXID(idCargo);
        }


    }
}
