﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLusuario
    {

        public List<BEUsuario> ListarUsuariosxPadre(Int32 strUsuarioPadre, String strApellido)
        {
            DACUsuario objDAO = new DACUsuario();
            return objDAO.ListarUsuariosxPadre(strUsuarioPadre, strApellido);
        }

        public List<BEUsuario> ListarUsuario(BEUsuario obj)
        {
            DACUsuario objDAO = new DACUsuario();
            return objDAO.ListarUsuario(obj);
        }

        public void GrabarDatos(BEUsuario obj)
        {
            DACUsuario objDAO = new DACUsuario();
            objDAO.GrabarDatos(obj);
        }

        public void EliminarUsuario(BEUsuario usuario)
        {
            DACUsuario objDAO = new DACUsuario();
            objDAO.EliminarUsuario(usuario);
        }

        public void UsuarioAceptaTC(int idUsuario, int idUsuarioActualizacion)
        {
            DACUsuario objDAO = new DACUsuario();
            objDAO.UsuarioAceptaTC(idUsuario, idUsuarioActualizacion);
        }
    }
}
