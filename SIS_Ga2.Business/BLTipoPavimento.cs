﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;
using System.Collections.Generic;


namespace SIS_Ga2.Business
{
    public class BLTipoPavimento
    {

        public List<BETipoPavimento> ListarTipoPavimento(int idTipoPavimento)
        {
            DACTipoPavimento objDAO = new DACTipoPavimento();
            return objDAO.ListarTipoPavimento(idTipoPavimento);
        }


    }
}
