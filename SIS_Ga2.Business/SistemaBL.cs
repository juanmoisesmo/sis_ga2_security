﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;

namespace SIS_Ga2.Business
{
    public class SistemaBL
    {
        public BEUsuario login(BEUsuario objEntidad)
        {
            SistemasDAO objDAO = new SistemasDAO();
            return objDAO.login(objEntidad);
        }
    }
}
