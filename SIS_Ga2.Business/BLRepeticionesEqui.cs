﻿using SIS_Ga2.DataAccess;
using SIS_Ga2.Entity;

namespace SIS_Ga2.Business
{
    public class BLRepeticionesEqui
    {

        public int GuardarRepeticionesEqui(BERepeticionesEqui objEntidad)
        {
            DACRepeticionesEqui objDAO = new DACRepeticionesEqui();
            return objDAO.GuardarRepeticionesEqui(objEntidad);
        }

        public int ActualizarRepeticionesEqui(BERepeticionesEqui objEntidad)

        {
            DACRepeticionesEqui objDAO = new DACRepeticionesEqui();
            return objDAO.ActualizarRepeticionesEqui(objEntidad);
        }
    }
}
