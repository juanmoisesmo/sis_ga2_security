﻿using DbManager.DataObjects;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace SIS_Ga2.DataAccess
{
    public class DACProyectos
    {

        public List<BEProyecto> ListarProyectosxId(int idProyecto)
        {
            try
            {
                Parameter param = new Parameter();
                param.Add("@idProyecto", idProyecto);
                SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
                List<BEProyecto> lista = objSql.getStatement<BEProyecto>("USP_ListarProyectosXId_Lst", param);
                return lista;
            }
            catch (Exception ex)
            {
                //Rutina de Guardado en Log 
                //afilogDAO.Save(0, 0, "CatalogoDAO", "GetCatalogoToCombo", ex);
                throw ex;
            }
        }

        public List<BEProyecto> ListarProyectos()
        {
            try
            {
                Parameter param = new Parameter();
                SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
                List<BEProyecto> lista = objSql.getStatement<BEProyecto>("USP_Proyectos_Lst", param);
                param = null;
                objSql.Dispose();
                objSql = null;
                return lista;
            }
            catch (Exception ex)
            {
                //Rutina de Guardado en Log 
                //afilogDAO.Save(0, 0, "CatalogoDAO", "GetCatalogoToCombo", ex);
                throw ex;
            }
        }

        public int GuardarCoeficientes(BECoefEstructura objEntidad)
        {
            SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
            int resultado = 0;
            Parameter param = new Parameter();
            param.Add("@Id_Diseno", objEntidad.idDiseno);
            param.Add("@Id_TipoPavimento", objEntidad.idTipoPavimento);
            param.Add("@Coeficiente_Drenaje_Calc", objEntidad.Coeficiente_Drenaje_Calc);
            param.Add("@Coeficiente_Drenaje_Ingresado", objEntidad.Coeficiente_Drenaje_Ingresado);
            param.Add("@Coeficiente_Estructural_Calc", objEntidad.Coeficiente_Estructural_Calc);
            param.Add("@Coeficiente_Estructural_Ingresado", objEntidad.Coeficiente_Estructural_Ingresado);
            param.Add("@Fecha_Creacion", objEntidad.FechaCreacion);
            param.Add("@Hora_Creacion", objEntidad.HoraCreacion);
            param.Add("@Usr_Creacion", objEntidad.UsrCreacion);

            try
            {
                objSql.ExecuteNonQuery("USP_Ins_Coeficientes", param);
                resultado = 1;
            }
            catch (Exception ex)
            {
                //Rutina de Guardado en Log 
                //afilogDAO.Save(0, 0, "CatalogoDAO", "GetCatalogoToCombo", ex);
                throw ex;
            }
            return resultado;
        }


        public void GrabarCoeficientes(BECoefEstructura objEntidad)
        {
            SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());

            Parameter param = new Parameter();

            param.Add("@Id_Diseno", objEntidad.idDiseno);
            param.Add("@Est_Capas_A1_Rec", objEntidad.Est_Capas_A1_Rec);
            param.Add("@Est_Capas_A1_Ing", objEntidad.Est_Capas_A1_Ing);
            param.Add("@Est_Capas_A2_Mos", objEntidad.Est_Capas_A2_Mos);
            param.Add("@Est_Capas_A2_Rec", objEntidad.Est_Capas_A2_Rec);
            param.Add("@Est_Capas_A2_Ing", objEntidad.Est_Capas_A2_Ing);
            param.Add("@Est_Capas_A3_Mos", objEntidad.Est_Capas_A3_Mos);
            param.Add("@Est_Capas_A3_Rec", objEntidad.Est_Capas_A3_Rec);
            param.Add("@Est_Capas_A3_Ing", objEntidad.Est_Capas_A3_Ing);
            param.Add("@Dren_Capas_M2_Calidad_Valor", objEntidad.Dren_Capas_M2_Calidad_Valor);
            param.Add("@Dren_Capas_M2_Porc_Sat", objEntidad.Dren_Capas_M2_Porc_Sat);
            param.Add("@Dren_Capas_M2_Coef_Dren", objEntidad.Dren_Capas_M2_Coef_Dren);
            param.Add("@Dren_Capas_M2_Coef_Dren_Ing", objEntidad.Dren_Capas_M2_Coef_Dren_Ing);
            param.Add("@Dren_Capas_M3_Calidad_Valor", objEntidad.Dren_Capas_M3_Calidad_Valor);
            param.Add("@Dren_Capas_M3_Porc_Sat", objEntidad.Dren_Capas_M3_Porc_Sat);
            param.Add("@Dren_Capas_M3_Coef_Dren", objEntidad.Dren_Capas_M3_Coef_Dren);
            param.Add("@Dren_Capas_M3_Coef_Dren_Ing", objEntidad.Dren_Capas_M3_Coef_Dren_Ing);
            param.Add("@SN_Propuesto", objEntidad.SN_Propuesto);
            param.Add("@SN_Requerido", objEntidad.SN_Requerido);
            param.Add("@Resultado_Capa_Asf", objEntidad.Resultado_Capa_Asf);
            param.Add("@Resultado_Base", objEntidad.Resultado_Base);
            param.Add("@Resultado_SubBase", objEntidad.Resultado_SubBase);
            param.Add("@UsuarioCreUpd", objEntidad.UsrCreacion);

            try
            {
                objSql.ExecuteNonQuery("Usp_InsUpd_CoeficientesProyecto", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
