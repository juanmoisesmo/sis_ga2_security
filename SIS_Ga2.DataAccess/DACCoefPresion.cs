﻿using DbManager.DataObjects;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace SIS_Ga2.DataAccess
{
    public class DACCoefPresion
    {
        public List<BECoefPresion> ListaCoefPresion()
        {
            try
            {
                Parameter param = new Parameter();

                SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
                List<BECoefPresion> lista = objSql.getStatement<BECoefPresion>("USP_Sel_Presion", param);
                return lista;
            }
            catch (Exception ex)
            {
                //Rutina de Guardado en Log 
                //afilogDAO.Save(0, 0, "CatalogoDAO", "GetCatalogoToCombo", ex);
                throw ex;
            }
        }



    }
}
