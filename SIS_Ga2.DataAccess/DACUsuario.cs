﻿using DbManager.DataObjects;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace SIS_Ga2.DataAccess
{
    public class DACUsuario
    {
        public List<BEUsuario> ListarUsuariosxPadre(Int32 strUsuarioPadre, String strApellido)
        {
            try
            {
                Parameter param = new Parameter();
                param.Add("@IDUsuarioPadre", strUsuarioPadre);
                param.Add("@Apellido", strApellido);
                SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
                List<BEUsuario> lista = objSql.getStatement<BEUsuario>("USP_Sel_UsuarioxPadre", param);
                return lista;
            }
            catch (Exception ex)
            {
                //Rutina de Guardado en Log 
                //afilogDAO.Save(0, 0, "CatalogoDAO", "GetCatalogoToCombo", ex);
                throw ex;
            }
        }

        public List<BEUsuario> ListarUsuario(BEUsuario usuario)
        {
            try
            {
    
                SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
                Parameter param = new Parameter();

                param.Add("@p_Dni", usuario.Dni);
                param.Add("@p_Apellido", usuario.Apellido);
                param.Add("@p_Nombre", usuario.Nombre);
                param.Add("@p_Id_Cargo", usuario.Id_Cargo);
                param.Add("@p_estado", usuario.Estado);

                List<BEUsuario> lista = objSql.getStatement<BEUsuario>("USP_USUARIO_LISTAR_TODO", param);

                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GrabarDatos(BEUsuario usuario)
        {
            SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
            Parameter param = new Parameter();

            param.Add("@P_Id_Usuario", usuario.Id_Usuario);
            param.Add("@P_Dni", usuario.Dni);
            param.Add("@P_Apellido", usuario.Apellido);
            param.Add("@P_Nombre", usuario.Nombre);
            param.Add("@P_Direccion", usuario.Direccion);
            param.Add("@P_Telefono", usuario.Telefono);
            param.Add("@P_Correo", usuario.Correo);
            param.Add("@P_Usuario", usuario.Usuario);
            param.Add("@P_Clave", usuario.Clave);
            param.Add("@P_Estado", usuario.Estado);
            param.Add("@P_Id_Cargo", usuario.Id_Cargo);
            param.Add("@P_Id_UsuarioInsUpd", usuario.Usr_Creacion);

            try
            {
                objSql.ExecuteNonQuery("USP_USUARIO_INS_UPD", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void EliminarUsuario(BEUsuario usuario)
        {
            SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
            Parameter param = new Parameter();

            param.Add("@P_ID_USUARIO", usuario.Id_Usuario);
            param.Add("@P_ID_USUARIO_UPDATE", usuario.Usr_Actualizacion);
            param.Add("@P_ESTADO", usuario.Estado.ToString());

            try
            {
                objSql.ExecuteNonQuery("USP_USUARIO_ELIMINAR", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void UsuarioAceptaTC(int idUsuario, int idUsuarioActualizacion)
        {
            SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
            Parameter param = new Parameter();

            param.Add("@p_IdUsuario", idUsuario);
            param.Add("@p_IdUsuarioUpd", idUsuarioActualizacion);

            try
            {
                objSql.ExecuteNonQuery("USP_USUARIO_ACEPTA_TC", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
