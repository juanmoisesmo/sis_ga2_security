﻿using DbManager.DataObjects;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
namespace SIS_Ga2.DataAccess
{
    public class DACCalidadDre
    {
        public List<BECalidadDre> ListarCaliDrenajesXID(int IdCalidadDre)
        {
            try
            {
                Parameter param = new Parameter();
                param.Add("@Id_Calidad_Drena", IdCalidadDre);
                SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
                List<BECalidadDre> lista = objSql.getStatement<BECalidadDre>("USP_Sel_Cal_Drenaje", param);
                return lista;
            }
            catch (Exception ex)
            {
                //Rutina de Guardado en Log 
                //afilogDAO.Save(0, 0, "CatalogoDAO", "GetCatalogoToCombo", ex);
                throw ex;
            }
        }
    }
}
