﻿using DbManager.DataObjects;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SIS_Ga2.DataAccess
{
    public class DACProyectoDiseno
    {
        public long GuardarProyectoDiseno(string jsonData, string usuario, int tipodiseno, long iddiseno)
        {
            SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());

            Parameter param = new Parameter();
            param.Add("@p_Id_Diseno", iddiseno);
            param.Add("@p_Json_Data", jsonData);
            param.Add("@p_Tipo_Diseno", tipodiseno);
            param.Add("@p_Usuario", usuario);
            param.Add("@p_Id_Diseno_out", 0, System.Data.ParameterDirection.Output);

            try
            {
                objSql.ExecuteNonQuery("usp_grabar_proyecto_diseno", param);
                var resultado = Convert.ToInt64(param.get_Item(5).Value.ToString());

                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BEProyectoDiseno> ListarTodoProyectoDiseno(string usuario, int? tipo_diseno = null)
        {
            try
            {
                var param = new Parameter();
                param.Add("@p_Tipo_Diseno", tipo_diseno);
                param.Add("@p_Usuario", usuario);

                var objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());

                return objSql.getStatement<BEProyectoDiseno>("usp_listar_todos_proyecto_diseno", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BEProyectoDiseno ObtenerProyectoDiseno(int iddiseno)
        {
            try
            {
                var param = new Parameter();
                param.Add("@p_Id_Diseno", iddiseno);

                var objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());

                return objSql.getStatement<BEProyectoDiseno>("usp_obtener_proyecto_diseno", param).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizaEstadoProyectoDiseno(bool estado, string usuario, int iddiseno)
        {
            SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());

            Parameter param = new Parameter();
            param.Add("@p_Id_Diseno", iddiseno);
            param.Add("@p_Estado", estado);
            param.Add("@p_Usuario", usuario);

            try
            {
                objSql.ExecuteNonQuery("usp_actualiza_estado_proyecto_diseno", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
