﻿using DbManager.DataObjects;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
namespace SIS_Ga2.DataAccess
{
    public class DACTiempoElimAgua
    {
        public List<BETiempoElimAgua> ListarTiempoElimAguaXID(int IDTiempoElimAgua)
        {
            try
            {
                Parameter param = new Parameter();
                param.Add("@Id_Tiempo_Elim_Agua", IDTiempoElimAgua);
                SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
                List<BETiempoElimAgua> lista = objSql.getStatement<BETiempoElimAgua>("USP_Sel_Tiempo_ElimAgua", param);
                return lista;
            }
            catch (Exception ex)
            {
                //Rutina de Guardado en Log 
                //afilogDAO.Save(0, 0, "CatalogoDAO", "GetCatalogoToCombo", ex);
                throw ex;
            }
        }
    }
}
