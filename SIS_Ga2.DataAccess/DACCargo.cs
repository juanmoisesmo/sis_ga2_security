﻿using DbManager.DataObjects;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace SIS_Ga2.DataAccess
{
    public class DACCargo
    {
        public List<BECargo> ListarCargosxId(int idCargo)
        {
            try
            {
                Parameter param = new Parameter();
                param.Add("@idCargo", idCargo);
                SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
                List<BECargo> lista = objSql.getStatement<BECargo>("USP_ListarCargosxId_Lst", param);
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BECargo> ListarCargos()
        {
            try
            {
                Parameter param = new Parameter();
                SqlManager objSql = new SqlManager(ConfigurationManager.AppSettings["ASOCEM"].ToString());
                List<BECargo> lista = objSql.getStatement<BECargo>("USP_CARGO_LISTAR", param);
                param = null;
                objSql.Dispose();
                objSql = null;
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
