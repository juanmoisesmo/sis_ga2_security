﻿namespace SIS_Ga2.Entity
{
    public class BEUsuario
    {
        public int Id_Usuario { get; set; }
        public string Dni { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string Cip { get; set; }
        public string Usuario { get; set; }
        public string Clave { get; set; }
        public int? Estado { get; set; }
        public string Estado_Descripcion { get; set; }
        public int Id_Jerarquia { get; set; }
        public int? Id_Cargo { get; set; }
        public double Fecha_Creacion { get; set; }
        public double Hora_Creacion { get; set; }
        public string Usr_Creacion { get; set; }
        public double Fecha_Actualizacion { get; set; }
        public double Hora_Actualizacion { get; set; }
        public string Usr_Actualizacion { get; set; }
        public string Cargo { get; set; }
        public int Aplicacion { get; set; }

        public string EstadoValor { get; set; }

        public int AceptoTc { get; set; }
        
    }
}
