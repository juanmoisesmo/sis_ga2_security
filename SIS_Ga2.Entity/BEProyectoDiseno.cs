﻿using System;

namespace SIS_Ga2.Entity
{
    public class BEProyectoDiseno
    {
        public long Id_Diseno { get; set; }
        public string Json_Data { get; set; }
        public int Tipo_Diseno { get; set; }
        public string Usr_Creacion { get; set; }
        public DateTime Fecha_Creacion { get; set; }
        public string Usr_Actualizacion { get; set; }
        public DateTime Fecha_Actualizacion { get; set; }
    }
}
