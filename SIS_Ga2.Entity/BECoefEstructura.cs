﻿namespace SIS_Ga2.Entity
{
    public class BECoefEstructura
    {
        public int idCoeficiente { get; set; }
        public int idDiseno { get; set; }
        public int idTipoPavimento { get; set; }
        public double CoefDrenaje { get; set; }
        public double CoefEstructural { get; set; }

        public double Coeficiente_Drenaje_Calc { get; set; }
        public double Coeficiente_Drenaje_Ingresado { get; set; }
        public double Coeficiente_Estructural_Calc { get; set; }
        public double Coeficiente_Estructural_Ingresado { get; set; }
        public double FechaCreacion { get; set; }
        public double HoraCreacion { get; set; }
        public string UsrCreacion { get; set; }
        public double FechaActualizacion { get; set; }
        public double HoraActualizacion { get; set; }
        public string UsrActualizacion { get; set; }

        //
        public decimal Est_Capas_A1_Rec { get; set; }
        public decimal Est_Capas_A1_Ing { get; set; }
        public decimal Est_Capas_A2_Mos { get; set; }
        public decimal Est_Capas_A2_Rec { get; set; }
        public decimal Est_Capas_A2_Ing { get; set; }
        public decimal Est_Capas_A3_Mos { get; set; }
        public decimal Est_Capas_A3_Rec { get; set; }
        public decimal Est_Capas_A3_Ing { get; set; }
        public decimal Dren_Capas_M2_Calidad_Valor { get; set; }
        public decimal Dren_Capas_M2_Porc_Sat { get; set; }
        public decimal Dren_Capas_M2_Coef_Dren { get; set; }
        public decimal Dren_Capas_M2_Coef_Dren_Ing { get; set; }
        public decimal Dren_Capas_M3_Calidad_Valor { get; set; }
        public decimal Dren_Capas_M3_Porc_Sat { get; set; }
        public decimal Dren_Capas_M3_Coef_Dren { get; set; }
        public decimal Dren_Capas_M3_Coef_Dren_Ing { get; set; }
        public decimal SN_Propuesto { get; set; }
        public decimal SN_Requerido { get; set; }
        public decimal Resultado_Capa_Asf { get; set; }
        public decimal Resultado_Base { get; set; }
        public decimal Resultado_SubBase { get; set; }

    }
}
