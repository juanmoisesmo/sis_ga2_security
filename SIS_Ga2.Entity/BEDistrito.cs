﻿namespace SIS_Ga2.Entity
{
    public class BEDistrito
    {
        public int id_Distrito { get; set; }
        public string Distrito { get; set; }
        public int idProvincia { get; set; }
    }
}
