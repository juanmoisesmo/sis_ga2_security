﻿namespace SIS_Ga2.Entity
{
    public class BookViewModel
    {
        public long Id
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }
        public bool IsWritten
        {
            get;
            set;
        }
    }
}
