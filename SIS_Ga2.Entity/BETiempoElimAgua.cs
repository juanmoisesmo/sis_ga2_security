﻿namespace SIS_Ga2.Entity
{
    public class BETiempoElimAgua
    {

        public int Id_Tiempo_Elim_Agua { get; set; }
        public string EliminaTiempoElimAgua { get; set; }
        public string CalidadDrenaje { get; set; }

    }
}
