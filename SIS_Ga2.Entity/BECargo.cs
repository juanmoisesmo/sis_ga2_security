﻿namespace SIS_Ga2.Entity
{
    public class BECargo
    {
        public int Id_Cargo { get; set; }
        public string Cargo { get; set; }
        public string NIvel { get; set; }
        public double FechaCreacion { get; set; }
        public double HoraCreacion { get; set; }
        public string UsrCreacion { get; set; }
        public double FechaActualizacion { get; set; }
        public double HoraActualizacion { get; set; }
        public string UsrActualizacion { get; set; }
    }
}
