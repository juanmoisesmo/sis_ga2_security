﻿using System.Collections.Generic;

namespace SIS_Ga2.Models
{
    public class DisenoAsfaltoModel
    {
        public string id_diseno { get; set; }
        public DatosProyectoModel datos_proyecto { get; set; }
        public CapasPavimentoAsfaltoModel capas_pavimento { get; set; }
        public CoeficientesAsfaltoModel coeficientes { get; set; }
        public ParametrosAsfaltoModel parametros { get; set; }
    }

    public class ParametrosAsfaltoModel
    {
        public EEAsfaltoModel ee { get; set; }
        public ResilenciaAsfaltoModel resilencia { get; set; }
        public LlaveValorModel periodo_diseno { get; set; }
        public string confiabilidad { get; set; }
        public string error_comb_estandar { get; set; }
        public string desv_estandar_normal { get; set; }
        public string serv_inicial { get; set; }
        public string serv_final { get; set; }
        public string dif_servicialidad { get; set; }
    }

    public class EEAsfaltoModel
    {
        public string valor_ee { get; set; }
        public string periodo_diseno { get; set; }
        public string dias_ano { get; set; }
        public FPAsfaltoModel fp { get; set; }
        public FDXFCAsfaltoModel fd_x_fc { get; set; }
        public TasaCrecimientoAsfaltoModel tasa_crecimiento { get; set; }
        public List<ConfiguracionesVehiculoAsfaltoModel> configuraciones_vehiculo_ee { get; set; }
    }

    public class ConfiguracionesVehiculoAsfaltoModel
    {
        public string id_vehiculos { get; set; }
        public string imagen_vehiculo { get; set; }
        public LlaveValorModel tipo_vehiculo { get; set; }
        public LlaveValorModel vehiculo { get; set; }
        public string imd_base { get; set; }
        public string fvp { get; set; }
        public string valor_ee { get; set; }
        public List<EjesAsfaltoModel> ejes { get; set; }
    }

    public class EjesAsfaltoModel
    {
        public LlaveValorModel tipo { get; set; }
        public string peso { get; set; }
    }

    public class TasaCrecimientoAsfaltoModel
    {
        public string valor_selector { get; set; }
        public string valor_descripcion { get; set; }
        public ConstanteAsfaltoModel constante { get; set; }
        public VariableTiempoAsfaltoModel variable_tiempo { get; set; }
        public VariableVehiculoAsfaltoModel variable_vehiculo { get; set; }
    }

    public class VariableTiempoAsfaltoModel
    {
        public List<PeriodoVariableTiempoAsfaltoModel> periodos { get; set; }
        public string total_perc { get; set; }
    }

    public class VariableVehiculoAsfaltoModel
    {
        public List<PeriodoVariableVehiculoAsfaltoModel> periodos { get; set; }
        public string total_perc { get; set; }
    }

    public class PeriodoVariableTiempoAsfaltoModel
    {
        public string id { get; set; }
        public string periodo { get; set; }
        public string valor_r { get; set; }
    }

    public class PeriodoVariableVehiculoAsfaltoModel
    {
        public string id { get; set; }
        public string id_tipo_vehiculo { get; set; }
        public string tipo_vehiculo { get; set; }
        public string valor_r { get; set; }
    }

    public class ConstanteAsfaltoModel
    {
        public string valor_crecimiento { get; set; }
    }

    public class FPAsfaltoModel
    {
        public string espesor { get; set; }
        public string presion { get; set; }
        public string valor_resultado { get; set; }
    }

    public class FDXFCAsfaltoModel
    {
        public string n_calzadas { get; set; }
        public string n_sentidos { get; set; }
        public string carril_sentido { get; set; }
        public string valor_resultado { get; set; }
        public string id_prop_factor_distrib { get; set; }
    }

    public class ResilenciaAsfaltoModel
    {
        public string valor { get; set; }
        public string valor_cbr { get; set; }
        public string valor_mr { get; set; }
    }

    public class CapasPavimentoAsfaltoModel
    {
        public string valor_me { get; set; }
        public string valor_cbr1 { get; set; }
        public string valor_cbr2 { get; set; }
        public string valor_cbr3 { get; set; }
        public string valor_modulo_rotura_psi { get; set; }
        public string valor_n18_nominal { get; set; }
        public string valor_n18_calculado_1 { get; set; }
        public string valor_n18_calculado_2 { get; set; }
        public string valor_sn_requerido { get; set; }
    }

    public class CoeficientesAsfaltoModel
    {
        public EstructurasCapasAsfaltoModel estructura_capas { get; set; }
        public DrenajeCapasAsfaltoModel drenaje_capas { get; set; }
        public string sn_propuesto { get; set; }
        public string sn_requerido { get; set; }
        public string resultado_capa_asf { get; set; }
        public string resultado_base { get; set; }
        public string resultado_sub_base { get; set; }

    }

    public class EstructurasCapasAsfaltoModel
    {
        public ConcretoAsfaltoModel concreto_asfalto { get; set; }
        public BaseAngularA2AsfaltoModel base_granular_a2 { get; set; }
        public SubBaseA3AsfaltoModel sub_base_a3 { get; set; }
    }

    public class ConcretoAsfaltoModel
    {
        public string valor_ingresado { get; set; }
        public string valor_estandar { get; set; }
        public string valor_mostrado { get; set; }
    }

    public class BaseAngularA2AsfaltoModel
    {
        public string valor_a2 { get; set; }
        public string valor_ingresado { get; set; }
        public string valor_mostrado { get; set; }
    }

    public class SubBaseA3AsfaltoModel
    {
        public string valor_a3 { get; set; }
        public string valor_ingresado { get; set; }
        public string valor_mostrado { get; set; }
    }

    public class DrenajeCapasAsfaltoModel
    {
        public string valor_base_granular_m2 { get; set; }
        public string valor_sub_base_m3 { get; set; }
        public CoeficientesDrenajeAsfaltoModel coeficientes { get; set; }
    }

    public class CoeficientesDrenajeAsfaltoModel
    {
        public string valor_dren_m2 { get; set; }
        public string valor_dren_m3 { get; set; }
        public BaseAngularM2AsfaltoModel base_granular_m2 { get; set; }
        public SubBaseM3AsfaltoModel sub_base_m3 { get; set; }
    }

    public class BaseAngularM2AsfaltoModel
    {
        public string valor_selector { get; set; }
        public string valor_descripcion { get; set; }
        public string porc_expo_saturacion { get; set; }
        public string coef_dren_b_gran { get; set; }
    }

    public class SubBaseM3AsfaltoModel
    {
        public string valor_selector { get; set; }
        public string valor_descripcion { get; set; }
        public string porc_expo_saturacion { get; set; }
        public string coef_dren_sub_b_gran { get; set; }
    }
}

