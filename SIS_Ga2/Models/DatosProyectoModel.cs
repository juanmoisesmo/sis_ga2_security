﻿namespace SIS_Ga2.Models
{
    public class DatosProyectoModel
    {
        public LlaveValorModel tipo_diseno { get; set; }
        public string fecha_proyecto { get; set; }
        public string fecha_contrato { get; set; }
        public string numero_proyecto { get; set; }
        public string proyecto { get; set; }
        public string diseno { get; set; }
        public string ingeniero { get; set; }
        public string tramo { get; set; }
        public LlaveValorModel reglamento { get; set; }
        public LlaveValorModel departamento { get; set; }
        public LlaveValorModel provincia { get; set; }
        public LlaveValorModel distrito { get; set; }
    }


}