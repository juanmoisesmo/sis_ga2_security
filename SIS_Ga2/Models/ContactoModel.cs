﻿namespace SIS_Ga2.Models
{
    public class ContactoModel
    {
        public string Nombre { get; set; }

        public string Correo { get; set; }

        public string Telefono { get; set; }

        public string Asunto { get; set; }

        public string Mensaje { get; set; }
    }
}