﻿using System.Collections.Generic;

namespace SIS_Ga2.Models
{
    public class DisenoAdoquinModel
    {
        public string id_diseno { get; set; }
        public DatosProyectoModel datos_proyecto { get; set; }
        public CapasPavimentoAdoquinModel capas_pavimento { get; set; }
        public CoeficientesAdoquinModel coeficientes { get; set; }
        public ParametrosAdoquinModel parametros { get; set; }
    }

    public class ParametrosAdoquinModel
    {
        public EEAdoquinModel ee { get; set; }
        public ResilenciaAdoquinModel resilencia { get; set; }
        public LlaveValorModel periodo_diseno { get; set; }
        public string confiabilidad { get; set; }
        public string error_comb_estandar { get; set; }
        public string desv_estandar_normal { get; set; }
        public string serv_inicial { get; set; }
        public string serv_final { get; set; }
        public string dif_servicialidad { get; set; }
    }

    public class EEAdoquinModel
    {
        public string valor_ee { get; set; }
        public string periodo_diseno { get; set; }
        public string dias_ano { get; set; }
        public FPAdoquinModel fp { get; set; }
        public FDXFCAdoquinModel fd_x_fc { get; set; }
        public TasaCrecimientoAdoquinModel tasa_crecimiento { get; set; }
        public List<ConfiguracionesVehiculoAdoquinModel> configuraciones_vehiculo_ee { get; set; }
    }

    public class ConfiguracionesVehiculoAdoquinModel
    {
        public string id_vehiculos { get; set; }
        public string imagen_vehiculo { get; set; }
        public LlaveValorModel tipo_vehiculo { get; set; }
        public LlaveValorModel vehiculo { get; set; }
        public string imd_base { get; set; }
        public string fvp { get; set; }
        public string valor_ee { get; set; }
        public List<EjesAdoquinModel> ejes { get; set; }
    }

    public class EjesAdoquinModel
    {
        public LlaveValorModel tipo { get; set; }
        public string peso { get; set; }
    }

    public class TasaCrecimientoAdoquinModel
    {
        public string valor_selector { get; set; }
        public string valor_descripcion { get; set; }
        public ConstanteAdoquinModel constante { get; set; }
        public VariableTiempoAdoquinModel variable_tiempo { get; set; }
        public VariableVehiculoAdoquinModel variable_vehiculo { get; set; }
    }

    public class VariableTiempoAdoquinModel
    {
        public List<PeriodoVariableTiempoAdoquinModel> periodos { get; set; }
        public string total_perc { get; set; }
    }

    public class VariableVehiculoAdoquinModel
    {
        public List<PeriodoVariableVehiculoAdoquinModel> periodos { get; set; }
        public string total_perc { get; set; }
    }

    public class PeriodoVariableTiempoAdoquinModel
    {
        public string id { get; set; }
        public string periodo { get; set; }
        public string valor_r { get; set; }
    }

    public class PeriodoVariableVehiculoAdoquinModel
    {
        public string id { get; set; }
        public string id_tipo_vehiculo { get; set; }
        public string tipo_vehiculo { get; set; }
        public string valor_r { get; set; }
    }

    public class ConstanteAdoquinModel
    {
        public string valor_crecimiento { get; set; }
    }

    public class FPAdoquinModel
    {
        public string espesor { get; set; }
        public string presion { get; set; }
        public string valor_resultado { get; set; }
    }

    public class FDXFCAdoquinModel
    {
        public string n_calzadas { get; set; }
        public string n_sentidos { get; set; }
        public string carril_sentido { get; set; }
        public string valor_resultado { get; set; }
        public string id_prop_factor_distrib { get; set; }
    }

    public class ResilenciaAdoquinModel
    {
        public string valor { get; set; }
        public string valor_cbr { get; set; }
        public string valor_mr { get; set; }
    }

    public class CapasPavimentoAdoquinModel
    {
        public string valor_me { get; set; }
        public string valor_cbr1 { get; set; }
        public string valor_cbr2 { get; set; }
        public string valor_cbr3 { get; set; }
        public string valor_modulo_rotura_psi { get; set; }
        public string valor_n18_nominal { get; set; }
        public string valor_n18_calculado_1 { get; set; }
        public string valor_n18_calculado_2 { get; set; }
        public string valor_sn_requerido { get; set; }
    }

    public class CoeficientesAdoquinModel
    {
        public EstructurasCapasAdoquinModel estructura_capas { get; set; }
        public DrenajeCapasAdoquinModel drenaje_capas { get; set; }
        public string sn_propuesto { get; set; }
        public string sn_requerido { get; set; }
        public string resultado_capa_adoquin { get; set; }
        public string resultado_capa_arena { get; set; }
        public string resultado_base { get; set; }
        public string resultado_sub_base { get; set; }

    }

    public class EstructurasCapasAdoquinModel
    {
        public ConcretoAdoquinModel concreto_asfalto { get; set; }
        public BaseAngularA2AdoquinModel base_granular_a2 { get; set; }
        public SubBaseA3AdoquinModel sub_base_a3 { get; set; }
    }

    public class ConcretoAdoquinModel
    {
        public string valor_ingresado { get; set; }
        public string valor_estandar { get; set; }
        public string valor_mostrado { get; set; }
    }

    public class BaseAngularA2AdoquinModel
    {
        public string valor_a2 { get; set; }
        public string valor_ingresado { get; set; }
        public string valor_mostrado { get; set; }
    }

    public class SubBaseA3AdoquinModel
    {
        public string valor_a3 { get; set; }
        public string valor_ingresado { get; set; }
        public string valor_mostrado { get; set; }
    }

    public class DrenajeCapasAdoquinModel
    {
        public string valor_base_granular_m2 { get; set; }
        public string valor_sub_base_m3 { get; set; }
        public CoeficientesDrenajeAdoquinModel coeficientes { get; set; }
    }

    public class CoeficientesDrenajeAdoquinModel
    {
        public string valor_dren_m2 { get; set; }

        public string valor_dren_m3 { get; set; }
        public BaseAngularM2AdoquinModel base_granular_m2 { get; set; }
        public SubBaseM3AdoquinModel sub_base_m3 { get; set; }
    }

    public class BaseAngularM2AdoquinModel
    {
        public string valor_selector { get; set; }
        public string valor_descripcion { get; set; }
        public string porc_expo_saturacion { get; set; }
        public string coef_dren_b_gran { get; set; }
    }

    public class SubBaseM3AdoquinModel
    {
        public string valor_selector { get; set; }
        public string valor_descripcion { get; set; }
        public string porc_expo_saturacion { get; set; }
        public string coef_dren_sub_b_gran { get; set; }
    }
}

