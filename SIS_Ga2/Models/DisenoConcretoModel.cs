﻿using System.Collections.Generic;

namespace SIS_Ga2.Models
{
    public class DisenoConcretoModel
    {
        public string id_diseno { get; set; }
        public DatosProyectoModel datos_proyecto { get; set; }
        public ParametrosConcretoModel parametros { get; set; }
        public CapasPavimentoConcretoModel capas_pavimento { get; set; }
    }


    public class ParametrosConcretoModel
    {
        public EEConcretoModel ee { get; set; }
        public LlaveValorModel periodo_diseno { get; set; }
        public string confiabilidad { get; set; }
        public string error_comb_estandar { get; set; }
        public string desv_estandar_normal { get; set; }
        public string serv_inicial { get; set; }
        public string serv_final { get; set; }
        public string dif_servicialidad { get; set; }
        public string coeficiente_transferencia { get; set; }
        public RoturaConcretoModel rotura { get; set; }
        public ReaccionConcretoModel reaccion { get; set; }
        public CoeficienteDreanjeConcretoModel coeficiente_drenaje { get; set; }
    }

    public class ReaccionConcretoModel
    {
        public string valor_mostrado { get; set; }
        public CbrCapasConcretoModel cbr_capas { get; set; }
        public KEquivalenteConcretoModel k_equivalente { get; set; }
    }

    public class CbrCapasConcretoModel
    {
        public string valor_subbase { get; set; }
        public string valor_subrasante { get; set; }
        public string valor_h { get; set; }
    }

    public class KEquivalenteConcretoModel
    {
        public string valor_k1_mpa { get; set; }
        public string valor_k1_kg { get; set; }
        public string valor_ko_mpa { get; set; }
        public string valor_ko_kg { get; set; }
        public string valor_keq_mpa { get; set; }
        public string valor_keq_kg { get; set; }
    }

    public class CoeficienteDreanjeConcretoModel
    {
        public string valor_mostrado { get; set; }
        public string valor_selector { get; set; }
        public string valor_descripcion { get; set; }
        public string porc_expo_saturacion { get; set; }
        public string coef_dren_b_gran { get; set; }
        public string valor_dren_m2 { get; set; }
    }

    public class RoturaConcretoModel
    {
        public string valor_mostrado { get; set; }
        public string resistencia_compresion_mr_valor { get; set; }
        public string resistencia_compresion_mr_descripcion { get; set; }
        public string resistencia_compresion_fc { get; set; }
        public string elasticidad { get; set; }
    }

    public class EEConcretoModel
    {
        public string valor_ee { get; set; }
        public string periodo_diseno { get; set; }
        public string dias_ano { get; set; }
        public FPConcretoModel fp { get; set; }
        public FDXFCConcretoModel fd_x_fc { get; set; }
        public TasaCrecimientoConcretoModel tasa_crecimiento { get; set; }
        public List<ConfiguracionesVehiculoConcretoModel> configuraciones_vehiculo_ee { get; set; }
    }

    public class ConfiguracionesVehiculoConcretoModel
    {
        public string id_vehiculos { get; set; }
        public string imagen_vehiculo { get; set; }
        public LlaveValorModel tipo_vehiculo { get; set; }
        public LlaveValorModel vehiculo { get; set; }
        public string imd_base { get; set; }
        public string fvp { get; set; }
        public string valor_ee { get; set; }
        public List<EjesConcretoModel> ejes { get; set; }
    }

    public class EjesConcretoModel
    {
        public LlaveValorModel tipo { get; set; }
        public string peso { get; set; }
    }

    public class TasaCrecimientoConcretoModel
    {
        public string valor_selector { get; set; }
        public string valor_descripcion { get; set; }
        public ConstanteConcretoModel constante { get; set; }
        public VariableTiempoConcretoModel variable_tiempo { get; set; }
        public VariableVehiculoConcretoModel variable_vehiculo { get; set; }
    }

    public class VariableTiempoConcretoModel
    {
        public List<PeriodoVariableTiempoConcretoModel> periodos { get; set; }
        public string total_perc { get; set; }
    }

    public class VariableVehiculoConcretoModel
    {
        public List<PeriodoVariableVehiculoConcretoModel> periodos { get; set; }
        public string total_perc { get; set; }
    }

    public class PeriodoVariableTiempoConcretoModel
    {
        public string id { get; set; }
        public string periodo { get; set; }
        public string valor_r { get; set; }
    }

    public class PeriodoVariableVehiculoConcretoModel
    {
        public string id { get; set; }
        public string id_tipo_vehiculo { get; set; }
        public string tipo_vehiculo { get; set; }
        public string valor_r { get; set; }
    }

    public class ConstanteConcretoModel
    {
        public string valor_crecimiento { get; set; }
    }

    public class FPConcretoModel
    {
        public string espesor { get; set; }
        public string presion { get; set; }
        public string valor_resultado { get; set; }
    }

    public class FDXFCConcretoModel
    {
        public string n_calzadas { get; set; }
        public string n_sentidos { get; set; }
        public string carril_sentido { get; set; }
        public string valor_resultado { get; set; }
        public string id_prop_factor_distrib { get; set; }
    }

    public class ResilenciaConcretoModel
    {
        public string valor { get; set; }
        public string valor_cbr { get; set; }
        public string valor_mr { get; set; }
    }

    public class CapasPavimentoConcretoModel
    {
        public string valor_fc_concreto { get; set; }
        public string valor_cbr1 { get; set; }
        public string valor_cbr2 { get; set; }
        public string valor_n18_nominal_calculado { get; set; }
        public string valor_d_requerido { get; set; }
        public string resultado_capa_concreto { get; set; }
        public string resultado_subbase { get; set; }
    }

}