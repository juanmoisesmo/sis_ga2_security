﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Newtonsoft.Json;
using SIS_Ga2.Business;
using SIS_Ga2.Entity;
using SIS_Ga2.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace SIS_Ga2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            BLDepartamento objblDepartamento = new BLDepartamento();
            List<SelectListItem> data_list = new List<SelectListItem>();
            data_list.AddRange(objblDepartamento.ListarDepartamentos(0).Select(a => new SelectListItem() { Text = a.Departamento.ToUpper(), Value = Convert.ToString(a.Id_Departamento) }));
            ViewData["ddlDepartamento"] = data_list;
            ViewData["ddlProvincia"] = new SelectList(new[] { "(Selecciona)" });
            ViewData["ddlDistrito"] = new SelectList(new[] { "(Selecciona)" });

            var lsttipodiseno = new List<SelectListItem>
            {
                new SelectListItem { Text = "TODOS", Value = "0" },
                new SelectListItem { Text = "DISEÑO ASFALTO", Value = "1" },
                new SelectListItem { Text = "DISEÑO ADOQUIN", Value = "2" },
                new SelectListItem { Text = "DISEÑO CONCRETO", Value = "3" }
            };
            ViewData["ddlTipoDiseno"] = lsttipodiseno;
            return View();
        }

        [HttpPost]
        public ActionResult FormRegistraProyecto()
        {

            Proyecto Proyecto = new Proyecto();
            ProyectosController RegistraProyecto = new ProyectosController();

            Proyecto.CodProyecto = Request.Form["frmProyecto"];
            Proyecto.NumDiseno = Request.Form["frmDiseno"];
            Proyecto.Reglamento = Request.Form["frmReglamento"];

            Session.Add("sistema.proyecto", Proyecto);


            return RedirectToAction("Index", "Diseno");
        }

        public JsonResult ListaProvincias(int idDepartamento)
        {

            BLProvincia objblProvincia = new BLProvincia();
            List<BEProvincia> lobjProvincia = new List<BEProvincia>();
            lobjProvincia = objblProvincia.ListarProvincia(idDepartamento);

            if (lobjProvincia == null)
                throw new ArgumentException("Departamento " + idDepartamento + " no es correcta");
            return Json(lobjProvincia);
        }

        public JsonResult ListaDistritos(int idProvincia)
        {

            List<BEDistrito> lobjbeDistrito = new List<BEDistrito>();
            BLDistrito objDistrito = new BLDistrito();

            lobjbeDistrito = objDistrito.ListarDistrito(idProvincia);
            if (lobjbeDistrito == null)
                throw new ArgumentException("Distrito " + idProvincia + " no es correcta");
            return Json(lobjbeDistrito);
        }

        public JsonResult ListaUsuarios(Int32 strUsuario, String valorUsuario)
        {
            BLusuario objBLusuario = new BLusuario();
            List<BEUsuario> lobjBeUsuario = new List<BEUsuario>();

            lobjBeUsuario = objBLusuario.ListarUsuariosxPadre(strUsuario, valorUsuario);

            if (lobjBeUsuario == null)
                throw new ArgumentException("Usuarios " + strUsuario + " no es correcta");
            return Json(lobjBeUsuario);
        }

        public ActionResult NuevoProyecto()
        {
            var proyecto = Session["proyecto.general"] as BEProyecto;
            proyecto.Id_Diseno = 0;
            Session["proyecto.general"] = proyecto;


            return RedirectToAction("Index", "ProyectoDiseno");
        }


        public int EliminarProyDiseno(Int32 Id_Diseno, Int32 Id_Usuario)
        {
            var blproyectodiseno = new BLProyectoDiseno();
            blproyectodiseno.ActualizaEstadoProyectoDiseno(false, Id_Usuario.ToString(), Id_Diseno);

            return 1;
        }

        public ActionResult EditarProyDiseno(Int32 Id_Diseno, Int32 Id_TipoDiseno)
        {
            var proyecto = Session["proyecto.general"] as BEProyecto;

            proyecto.Id_Diseno = Id_Diseno;
            proyecto.Id_TipoDiseno = Id_TipoDiseno;
            Session["proyecto.general"] = proyecto;

            var sistema = Session["sistema.general"] as Sistema;
            sistema.idTipoDiseno = Id_TipoDiseno;
            sistema.idAplicacion = Id_TipoDiseno;
            sistema.TipoDiseno = Id_TipoDiseno == 1 ? "ASFALTO" : Id_TipoDiseno == 2 ? "ADOQUIN" : "CONCRETO";
            Session["sistema.general"] = sistema;

            return RedirectToAction("Index", "ProyectoDiseno");
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(int idtipodiseno, int iddiseno)
        {
            if (idtipodiseno == 1)
                return GenerarPDFReporteParametrosAsfalto(iddiseno);
            else if (idtipodiseno == 2)
                return GenerarPDFReporteParametrosAdoquin(iddiseno);
            else
                return GenerarPDFReporteParametrosConcreto(iddiseno);
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult ExportComparativo(int iddisenoasf, int iddisenoado, int iddisenocon)
        {
            BEProyectoDiseno asfproyectodiseno = null;
            BEProyectoDiseno adoproyectodiseno = null;
            BEProyectoDiseno conproyectodiseno = null;

            var blproyectodiseno = new BLProyectoDiseno();

            if (iddisenoasf != 0)
                asfproyectodiseno = blproyectodiseno.ObtenerProyectoDiseno(iddisenoasf);

            if (iddisenoado != 0)
                adoproyectodiseno = blproyectodiseno.ObtenerProyectoDiseno(iddisenoado);

            if (iddisenocon != 0)
                conproyectodiseno = blproyectodiseno.ObtenerProyectoDiseno(iddisenocon);

            return GenerarReporteComparativo(asfproyectodiseno, adoproyectodiseno, conproyectodiseno);
        }

        private FileResult GenerarPDFReporteParametrosAsfalto(int iddiseno)
        {
            var blproyectodiseno = new BLProyectoDiseno();

            var resultado = blproyectodiseno.ObtenerProyectoDiseno(iddiseno);

            var modelostr = resultado.Json_Data;
            var modelo = JsonConvert.DeserializeObject<DisenoAsfaltoModel>(modelostr);
            var tablarecurso = ConstantesRecursos.TablaDisenoReporteAsfalto;

            var ubicacion = string.Format("{0}/{1}/{2}", modelo.datos_proyecto.departamento.valor_descripcion, modelo.datos_proyecto.provincia.valor_descripcion, modelo.datos_proyecto.distrito.valor_descripcion);

            tablarecurso = tablarecurso.Replace("[Proyecto]", modelo.datos_proyecto.proyecto);
            tablarecurso = tablarecurso.Replace("[Tramo]", modelo.datos_proyecto.tramo);
            tablarecurso = tablarecurso.Replace("[Fecha]", modelo.datos_proyecto.fecha_proyecto);
            tablarecurso = tablarecurso.Replace("[Ubicación]", ubicacion);
            tablarecurso = tablarecurso.Replace("[Ingeniero]", modelo.datos_proyecto.ingeniero);
            tablarecurso = tablarecurso.Replace("[Reglamento]", modelo.datos_proyecto.reglamento.valor_descripcion);
            tablarecurso = tablarecurso.Replace("[Diseno]", modelo.datos_proyecto.diseno);
            tablarecurso = tablarecurso.Replace("[TipoDiseno]", modelo.datos_proyecto.tipo_diseno.valor_descripcion);
            tablarecurso = tablarecurso.Replace("[Periodo]", (modelo.parametros.periodo_diseno.valor_descripcion + " años"));
            tablarecurso = tablarecurso.Replace("[EE]", modelo.parametros.ee.valor_ee);
            tablarecurso = tablarecurso.Replace("[CONFIABILIDAD]", modelo.parametros.confiabilidad + " %");
            tablarecurso = tablarecurso.Replace("[COMBINACIÓN]", modelo.parametros.error_comb_estandar);
            tablarecurso = tablarecurso.Replace("[RESILENCIA]", modelo.parametros.resilencia.valor_mr + " psi");
            tablarecurso = tablarecurso.Replace("[SERVICIABILIDAD_I]", modelo.parametros.serv_inicial);
            tablarecurso = tablarecurso.Replace("[SERVICIABILIDAD_F]", modelo.parametros.serv_final);
            tablarecurso = tablarecurso.Replace("[DIFERENCIA]", modelo.parametros.dif_servicialidad);
            tablarecurso = tablarecurso.Replace("[DESVIACIÓN]", modelo.parametros.desv_estandar_normal);

            tablarecurso = tablarecurso.Replace("[a1]", modelo.coeficientes.estructura_capas.concreto_asfalto.valor_mostrado);
            tablarecurso = tablarecurso.Replace("[a2]", modelo.coeficientes.estructura_capas.base_granular_a2.valor_mostrado);
            tablarecurso = tablarecurso.Replace("[a3]", modelo.coeficientes.estructura_capas.sub_base_a3.valor_mostrado);
            tablarecurso = tablarecurso.Replace("[b2]", Convert.ToDouble(modelo.coeficientes.drenaje_capas.coeficientes.valor_dren_m2).ToString("N2"));
            tablarecurso = tablarecurso.Replace("[b3]", Convert.ToDouble(modelo.coeficientes.drenaje_capas.coeficientes.valor_dren_m3).ToString("N2"));

            var espasf = Convert.ToDouble(modelo.coeficientes.resultado_capa_asf) / 2.54;
            tablarecurso = tablarecurso.Replace("[c1]", Math.Round(espasf, 2).ToString());

            var sncapasf = Convert.ToDouble(modelo.coeficientes.estructura_capas.concreto_asfalto.valor_mostrado) * espasf;
            tablarecurso = tablarecurso.Replace("[d1]", Math.Round(sncapasf, 2).ToString());

            var espbas = Convert.ToDouble(modelo.coeficientes.resultado_base) / 2.54;
            tablarecurso = tablarecurso.Replace("[c2]", Math.Round(espbas, 2).ToString());

            var sncapabas = Convert.ToDouble(modelo.coeficientes.estructura_capas.base_granular_a2.valor_mostrado) * espbas * Convert.ToDouble(modelo.coeficientes.drenaje_capas.coeficientes.valor_dren_m2);
            tablarecurso = tablarecurso.Replace("[d2]", Math.Round(sncapabas, 2).ToString());

            var espsubbas = Convert.ToDouble(modelo.coeficientes.resultado_sub_base) / 2.54;
            tablarecurso = tablarecurso.Replace("[c3]", Math.Round(espsubbas, 2).ToString());

            var sncapasubbas = Convert.ToDouble(modelo.coeficientes.estructura_capas.sub_base_a3.valor_mostrado) * espsubbas * Convert.ToDouble(modelo.coeficientes.drenaje_capas.coeficientes.valor_dren_m3);
            tablarecurso = tablarecurso.Replace("[d3]", Math.Round(sncapasubbas, 2).ToString());

            var snpropt = sncapasf + sncapabas + sncapasubbas;
            tablarecurso = tablarecurso.Replace("[d4]", Math.Round(snpropt, 2).ToString());

            tablarecurso = tablarecurso.Replace("[e1]", modelo.coeficientes.resultado_capa_asf);
            tablarecurso = tablarecurso.Replace("[e2]", modelo.coeficientes.resultado_base);
            tablarecurso = tablarecurso.Replace("[e3]", modelo.coeficientes.resultado_sub_base);

            tablarecurso = tablarecurso.Replace("[f1]", modelo.coeficientes.resultado_capa_asf + " cm");
            tablarecurso = tablarecurso.Replace("[f2]", modelo.coeficientes.resultado_base + " cm");
            tablarecurso = tablarecurso.Replace("[f3]", modelo.coeficientes.resultado_sub_base + " cm");

            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(tablarecurso);
                Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 10f, 10f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "ReporteDisenoAsfalto.pdf");
            }
        }

        private FileResult GenerarPDFReporteParametrosAdoquin(int iddiseno)
        {
            var blproyectodiseno = new BLProyectoDiseno();

            var resultado = blproyectodiseno.ObtenerProyectoDiseno(iddiseno);

            var modelostr = resultado.Json_Data;
            var modelo = JsonConvert.DeserializeObject<DisenoAdoquinModel>(modelostr);
            var tablarecurso = ConstantesRecursos.TableDisenoReporteAdoquin;

            var ubicacion = string.Format("{0}/{1}/{2}", modelo.datos_proyecto.departamento.valor_descripcion, modelo.datos_proyecto.provincia.valor_descripcion, modelo.datos_proyecto.distrito.valor_descripcion);

            tablarecurso = tablarecurso.Replace("[Proyecto]", modelo.datos_proyecto.proyecto);
            tablarecurso = tablarecurso.Replace("[Tramo]", modelo.datos_proyecto.tramo);
            tablarecurso = tablarecurso.Replace("[Fecha]", modelo.datos_proyecto.fecha_proyecto);
            tablarecurso = tablarecurso.Replace("[Ubicación]", ubicacion);
            tablarecurso = tablarecurso.Replace("[Ingeniero]", modelo.datos_proyecto.ingeniero);
            tablarecurso = tablarecurso.Replace("[Reglamento]", modelo.datos_proyecto.reglamento.valor_descripcion);
            tablarecurso = tablarecurso.Replace("[Diseno]", modelo.datos_proyecto.diseno);
            tablarecurso = tablarecurso.Replace("[TipoDiseno]", modelo.datos_proyecto.tipo_diseno.valor_descripcion);
            tablarecurso = tablarecurso.Replace("[Periodo]", (modelo.parametros.periodo_diseno.valor_descripcion + " años"));
            tablarecurso = tablarecurso.Replace("[EE]", modelo.parametros.ee.valor_ee);
            tablarecurso = tablarecurso.Replace("[CONFIABILIDAD]", modelo.parametros.confiabilidad + " %");
            tablarecurso = tablarecurso.Replace("[COMBINACIÓN]", modelo.parametros.error_comb_estandar);
            tablarecurso = tablarecurso.Replace("[RESILENCIA]", modelo.parametros.resilencia.valor_mr + " psi");
            tablarecurso = tablarecurso.Replace("[SERVICIABILIDAD_I]", modelo.parametros.serv_inicial);
            tablarecurso = tablarecurso.Replace("[SERVICIABILIDAD_F]", modelo.parametros.serv_final);
            tablarecurso = tablarecurso.Replace("[DIFERENCIA]", modelo.parametros.dif_servicialidad);
            tablarecurso = tablarecurso.Replace("[DESVIACIÓN]", modelo.parametros.desv_estandar_normal);

            tablarecurso = tablarecurso.Replace("[a1]", modelo.coeficientes.estructura_capas.concreto_asfalto.valor_mostrado);
            tablarecurso = tablarecurso.Replace("[a2]", modelo.coeficientes.estructura_capas.base_granular_a2.valor_mostrado);
            tablarecurso = tablarecurso.Replace("[a3]", modelo.coeficientes.estructura_capas.sub_base_a3.valor_mostrado);
            tablarecurso = tablarecurso.Replace("[b2]", Convert.ToDouble(modelo.coeficientes.drenaje_capas.coeficientes.valor_dren_m2).ToString("N2"));
            tablarecurso = tablarecurso.Replace("[b3]", Convert.ToDouble(modelo.coeficientes.drenaje_capas.coeficientes.valor_dren_m3).ToString("N2"));

            var espado = Convert.ToDouble(modelo.coeficientes.resultado_capa_adoquin) / 2.54;
            var esparea = Convert.ToDouble(modelo.coeficientes.resultado_capa_arena) / 2.54;
            var espesora = espado + esparea;
            tablarecurso = tablarecurso.Replace("[c1]", Math.Round(espesora, 2).ToString());

            var sncapasf = Convert.ToDouble(modelo.coeficientes.estructura_capas.concreto_asfalto.valor_mostrado) * espesora;
            tablarecurso = tablarecurso.Replace("[d1]", Math.Round(sncapasf, 2).ToString());

            var espbas = Convert.ToDouble(modelo.coeficientes.resultado_base) / 2.54;
            tablarecurso = tablarecurso.Replace("[c2]", Math.Round(espbas, 2).ToString());

            var sncapabas = Convert.ToDouble(modelo.coeficientes.estructura_capas.base_granular_a2.valor_mostrado) * espbas * Convert.ToDouble(modelo.coeficientes.drenaje_capas.coeficientes.valor_dren_m2);
            tablarecurso = tablarecurso.Replace("[d2]", Math.Round(sncapabas, 2).ToString());

            var espsubbas = Convert.ToDouble(modelo.coeficientes.resultado_sub_base) / 2.54;
            tablarecurso = tablarecurso.Replace("[c3]", Math.Round(espsubbas, 2).ToString());

            var sncapasubbas = Convert.ToDouble(modelo.coeficientes.estructura_capas.sub_base_a3.valor_mostrado) * espsubbas * Convert.ToDouble(modelo.coeficientes.drenaje_capas.coeficientes.valor_dren_m3);
            tablarecurso = tablarecurso.Replace("[d3]", Math.Round(sncapasubbas, 2).ToString());

            var snpropt = sncapasf + sncapabas + sncapasubbas;
            tablarecurso = tablarecurso.Replace("[d4]", Math.Round(snpropt, 2).ToString());

            var espadocm = Convert.ToDouble(modelo.coeficientes.resultado_capa_adoquin);
            var espareacm = Convert.ToDouble(modelo.coeficientes.resultado_capa_arena);
            var espesoracm = espadocm + espareacm;

            tablarecurso = tablarecurso.Replace("[e1]", espesoracm.ToString("N2"));
            tablarecurso = tablarecurso.Replace("[e2]", modelo.coeficientes.resultado_base);
            tablarecurso = tablarecurso.Replace("[e3]", modelo.coeficientes.resultado_sub_base);

            tablarecurso = tablarecurso.Replace("[f1]", modelo.coeficientes.resultado_capa_adoquin + " cm");
            tablarecurso = tablarecurso.Replace("[f2]", modelo.coeficientes.resultado_capa_arena + " cm");
            tablarecurso = tablarecurso.Replace("[f3]", modelo.coeficientes.resultado_base + " cm");
            tablarecurso = tablarecurso.Replace("[f4]", modelo.coeficientes.resultado_sub_base + " cm");

            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(tablarecurso);
                Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 10f, 10f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "ReporteDisenoAdoquin.pdf");
            }
        }

        private FileResult GenerarPDFReporteParametrosConcreto(int iddiseno)
        {
            var blproyectodiseno = new BLProyectoDiseno();

            var resultado = blproyectodiseno.ObtenerProyectoDiseno(iddiseno);

            var modelostr = resultado.Json_Data;
            var modelo = JsonConvert.DeserializeObject<DisenoConcretoModel>(modelostr);
            var tablarecurso = ConstantesRecursos.TablaDisenoReporteConcreto;

            var ubicacion = string.Format("{0}/{1}/{2}", modelo.datos_proyecto.departamento.valor_descripcion, modelo.datos_proyecto.provincia.valor_descripcion, modelo.datos_proyecto.distrito.valor_descripcion);

            tablarecurso = tablarecurso.Replace("[Proyecto]", modelo.datos_proyecto.proyecto);
            tablarecurso = tablarecurso.Replace("[Tramo]", modelo.datos_proyecto.tramo);
            tablarecurso = tablarecurso.Replace("[Fecha]", modelo.datos_proyecto.fecha_proyecto);
            tablarecurso = tablarecurso.Replace("[Ubicación]", ubicacion);
            tablarecurso = tablarecurso.Replace("[Ingeniero]", modelo.datos_proyecto.ingeniero);
            tablarecurso = tablarecurso.Replace("[Reglamento]", modelo.datos_proyecto.reglamento.valor_descripcion);
            tablarecurso = tablarecurso.Replace("[Diseno]", modelo.datos_proyecto.diseno);
            tablarecurso = tablarecurso.Replace("[TipoDiseno]", modelo.datos_proyecto.tipo_diseno.valor_descripcion);
            tablarecurso = tablarecurso.Replace("[Periodo]", (modelo.parametros.periodo_diseno.valor_descripcion + " años"));
            tablarecurso = tablarecurso.Replace("[EE]", modelo.parametros.ee.valor_ee);
            tablarecurso = tablarecurso.Replace("[CONFIABILIDAD]", modelo.parametros.confiabilidad + " %");
            tablarecurso = tablarecurso.Replace("[DESVIACIÓN]", modelo.parametros.desv_estandar_normal);
            tablarecurso = tablarecurso.Replace("[COMBINACIÓN]", modelo.parametros.error_comb_estandar);
            tablarecurso = tablarecurso.Replace("[FC]", modelo.parametros.rotura.resistencia_compresion_fc + " Kg/cm2");
            tablarecurso = tablarecurso.Replace("[MR]", Convert.ToDouble(modelo.parametros.rotura.valor_mostrado).ToString("N2") + " Mpa");
            tablarecurso = tablarecurso.Replace("[EC]", Convert.ToDouble(modelo.parametros.rotura.elasticidad).ToString("N2") + " Mpa");
            tablarecurso = tablarecurso.Replace("[J]", modelo.parametros.coeficiente_transferencia);
            tablarecurso = tablarecurso.Replace("[KC]", Convert.ToDouble(modelo.parametros.reaccion.valor_mostrado).ToString("N2") + " Mpa/m");
            tablarecurso = tablarecurso.Replace("[CD]", modelo.parametros.coeficiente_drenaje.valor_mostrado);
            tablarecurso = tablarecurso.Replace("[SERVICIABILIDAD_I]", modelo.parametros.serv_inicial);
            tablarecurso = tablarecurso.Replace("[SERVICIABILIDAD_F]", modelo.parametros.serv_final);
            tablarecurso = tablarecurso.Replace("[DIFERENCIA]", modelo.parametros.dif_servicialidad);

            var espconcretopulgada = Convert.ToDouble(modelo.capas_pavimento.resultado_capa_concreto) / 2.54;
            tablarecurso = tablarecurso.Replace("[a2]", Math.Round(espconcretopulgada, 2).ToString());

            var espsubbasepulgada = Convert.ToDouble(modelo.capas_pavimento.resultado_subbase) / 2.54;
            tablarecurso = tablarecurso.Replace("[a3]", Math.Round(espsubbasepulgada, 2).ToString());

            tablarecurso = tablarecurso.Replace("[b2]", Convert.ToDouble(modelo.capas_pavimento.resultado_capa_concreto).ToString("N2"));
            tablarecurso = tablarecurso.Replace("[b3]", Convert.ToDouble(modelo.capas_pavimento.resultado_subbase).ToString("N2"));

            tablarecurso = tablarecurso.Replace("[f1]", Convert.ToDouble(modelo.capas_pavimento.resultado_capa_concreto).ToString("N2") + " cm");
            tablarecurso = tablarecurso.Replace("[f2]", Convert.ToDouble(modelo.capas_pavimento.resultado_subbase).ToString("N2") + " cm");


            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(tablarecurso);
                Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 10f, 10f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "ReporteDisenoConcreto.pdf");
            }
        }

        private FileResult GenerarReporteComparativo(BEProyectoDiseno asfproyectodiseno, BEProyectoDiseno adoproyectodiseno, BEProyectoDiseno conproyectodiseno)
        {
            var htmlcontent = "";

            if (asfproyectodiseno != null && adoproyectodiseno != null && conproyectodiseno != null)
                htmlcontent = ConstantesRecursos.TableComparativoDisenoTodos;
            else if (asfproyectodiseno != null && adoproyectodiseno != null)
                htmlcontent = ConstantesRecursos.TableComparativoDisenoAsfAdo;
            else if (asfproyectodiseno != null && conproyectodiseno != null)
                htmlcontent = ConstantesRecursos.TableComparativoDisenoAsfCon;
            else if (adoproyectodiseno != null && conproyectodiseno != null)
                htmlcontent = ConstantesRecursos.TableComparativoDisenoAdoCon;

            if (asfproyectodiseno != null)
            {
                var modelasfalto = JsonConvert.DeserializeObject<DisenoAsfaltoModel>(asfproyectodiseno.Json_Data);

                htmlcontent = htmlcontent.Replace("[PROYDIS_ASF]", modelasfalto.datos_proyecto.proyecto + " / " + modelasfalto.datos_proyecto.diseno);
                htmlcontent = htmlcontent.Replace("[REG_ASF]", modelasfalto.datos_proyecto.reglamento.valor_descripcion);
                htmlcontent = htmlcontent.Replace("[PD_ASF]", modelasfalto.parametros.periodo_diseno.valor_descripcion);
                htmlcontent = htmlcontent.Replace("[EE_ASF]", modelasfalto.parametros.ee.valor_ee);
                htmlcontent = htmlcontent.Replace("[CONF_ASF]", modelasfalto.parametros.confiabilidad);
                htmlcontent = htmlcontent.Replace("[PI_ASF]", modelasfalto.parametros.serv_inicial);
                htmlcontent = htmlcontent.Replace("[PT_ASF]", modelasfalto.parametros.serv_final);
                htmlcontent = htmlcontent.Replace("[CBR_ASF]", modelasfalto.capas_pavimento.valor_cbr3);
                htmlcontent = htmlcontent.Replace("[C1_ASF]", modelasfalto.coeficientes.resultado_capa_asf + "cm");
                htmlcontent = htmlcontent.Replace("[C2_ASF]", modelasfalto.coeficientes.resultado_base + "cm");
                htmlcontent = htmlcontent.Replace("[C3_ASF]", modelasfalto.coeficientes.resultado_sub_base + "cm");
            }


            if (adoproyectodiseno != null)
            {
                var modeladoquin = JsonConvert.DeserializeObject<DisenoAdoquinModel>(adoproyectodiseno.Json_Data);

                htmlcontent = htmlcontent.Replace("[PROYDIS_ADO]", modeladoquin.datos_proyecto.proyecto + " / " + modeladoquin.datos_proyecto.diseno);
                htmlcontent = htmlcontent.Replace("[REG_ADO]", modeladoquin.datos_proyecto.reglamento.valor_descripcion);
                htmlcontent = htmlcontent.Replace("[PD_ADO]", modeladoquin.parametros.periodo_diseno.valor_descripcion);
                htmlcontent = htmlcontent.Replace("[EE_ADO]", modeladoquin.parametros.ee.valor_ee);
                htmlcontent = htmlcontent.Replace("[CONF_ADO]", modeladoquin.parametros.confiabilidad);
                htmlcontent = htmlcontent.Replace("[PI_ADO]", modeladoquin.parametros.serv_inicial);
                htmlcontent = htmlcontent.Replace("[PT_ADO]", modeladoquin.parametros.serv_final);
                htmlcontent = htmlcontent.Replace("[CBR_ADO]", modeladoquin.capas_pavimento.valor_cbr3);
                htmlcontent = htmlcontent.Replace("[C1_ADO]", modeladoquin.coeficientes.resultado_capa_adoquin + "cm");
                htmlcontent = htmlcontent.Replace("[C2_ADO]", modeladoquin.coeficientes.resultado_capa_arena + "cm");
                htmlcontent = htmlcontent.Replace("[C3_ADO]", modeladoquin.coeficientes.resultado_base + "cm");
                htmlcontent = htmlcontent.Replace("[C4_ADO]", modeladoquin.coeficientes.resultado_sub_base + "cm");
            }


            if (conproyectodiseno != null)
            {
                var modelconcreto = JsonConvert.DeserializeObject<DisenoConcretoModel>(conproyectodiseno.Json_Data);

                htmlcontent = htmlcontent.Replace("[PROYDIS_CON]", modelconcreto.datos_proyecto.proyecto + " / " + modelconcreto.datos_proyecto.diseno);
                htmlcontent = htmlcontent.Replace("[REG_CON]", modelconcreto.datos_proyecto.reglamento.valor_descripcion);
                htmlcontent = htmlcontent.Replace("[PD_CON]", modelconcreto.parametros.periodo_diseno.valor_descripcion);
                htmlcontent = htmlcontent.Replace("[EE_CON]", modelconcreto.parametros.ee.valor_ee);
                htmlcontent = htmlcontent.Replace("[CONF_CON]", modelconcreto.parametros.confiabilidad);
                htmlcontent = htmlcontent.Replace("[MR_CON]", Convert.ToDecimal(modelconcreto.parametros.rotura.valor_mostrado).ToString("N2"));
                htmlcontent = htmlcontent.Replace("[J_CON]", modelconcreto.parametros.coeficiente_transferencia);
                htmlcontent = htmlcontent.Replace("[PI_CON]", modelconcreto.parametros.serv_inicial);
                htmlcontent = htmlcontent.Replace("[PT_CON]", modelconcreto.parametros.serv_final);
                htmlcontent = htmlcontent.Replace("[CBR_CON]", modelconcreto.capas_pavimento.valor_cbr2);
                htmlcontent = htmlcontent.Replace("[C1_CON]", modelconcreto.capas_pavimento.resultado_capa_concreto + "cm");
                htmlcontent = htmlcontent.Replace("[C2_CON]", modelconcreto.capas_pavimento.resultado_subbase + "cm");
            }

            using (MemoryStream stream = new MemoryStream())
            {
                StringReader sr = new StringReader(htmlcontent);
                Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
                pdfDoc.SetPageSize(PageSize.A4.Rotate());
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "ReporteComparativo.pdf");
            }
        }
    }
}