﻿using SIS_Ga2.Business;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;


namespace SIS_Ga2.Controllers
{
    public class SeguridadController : Controller
    {
        // GET: Seguridad
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string loginUsername, string loginPassword)
        {
            BEUsuario usuario = new BEUsuario();
            Sistema sistema = new Sistema();
            BEProyecto proyecto = new BEProyecto();

            usuario.Usuario = loginUsername;
            usuario.Clave = loginPassword;


            SistemaBL autorizacion = new SistemaBL();
            usuario = autorizacion.login(usuario);


            if (usuario.Usuario != null)
            {
                AplicacionBL objBL = new AplicacionBL();
                List<Aplicacion> Aplicaciones = objBL.ListarAplicaciones();

                sistema.cuenta = usuario.Usuario;
                sistema.clave = usuario.Clave;
                sistema.idUsuario = usuario.Id_Usuario;

                Session.Add("sistema.usuario", usuario);
                Session.Add("sistema.general", sistema);
                Session.Add("proyecto.general", proyecto);

                if (usuario.Id_Cargo == 4)
                    return RedirectToAction("Index", "Usuario");
                else
                {
                    if (usuario.AceptoTc == 1)
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    return RedirectToAction("Index", "TerminosCondiciones");

                }

            }

            ViewBag.Mensaje = "Usuario/Contraseña incorrecto";
            return View();

        }


        [HttpGet]
        public ActionResult Aplicaciones()
        {
            BEUsuario usuario = Session["sistema.usuario"] as BEUsuario;
            if (usuario == null) return RedirectToAction("Login", "Seguridad");

            return View();
        }

        [HttpPost]
        public ActionResult Aplicaciones(int IdAplicacion)
        {
            try
            {
                BEUsuario usuario = (BEUsuario)Session["sistema.usuario"];
                Sistema sistema = (Sistema)Session["sistema.general"];
                BEProyecto proyecto = (BEProyecto)Session["proyecto.general"];
                sistema.idAplicacion = IdAplicacion;
                usuario.Aplicacion = IdAplicacion;


                DateTime hoy = DateTime.Now;
                proyecto.Fecha_Proyecto_Date = hoy.ToString("dd/MM/yyyy");

                switch (IdAplicacion)
                {
                    case 1:
                        sistema.TipoDiseno = "ASFALTO";
                        sistema.idTipoDiseno = 1;
                        break;
                    case 2:
                        sistema.TipoDiseno = "ADOQUIN";
                        sistema.idTipoDiseno = 2;
                        break;
                    case 3:
                        sistema.TipoDiseno = "CONCRETO";
                        sistema.idTipoDiseno = 3;
                        break;
                }

                return RedirectToAction("NuevoProyecto", "Home");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            Session.Clear();
            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Seguridad");
        }

        public JsonResult MostrarAplicaciones()
        {
            try
            {
                AplicacionBL objBLApp = new AplicacionBL();
                List<Aplicacion> objLista = objBLApp.ListarAplicaciones();
                TempData.Remove("Seguridad.Aplicaciones");
                TempData.Add("Seguridad.Aplicaciones", objLista);

                Autorizacionusuario usuario = (Autorizacionusuario)Session["sistema.usuario"];
                var resultado = "OK";
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("NO-OK:" + ex.Message);
            }

        }


        public string SendEmail(string EmailDestino)
        {
            string emailCript;

            try
            {
                var bLusuario = new BLusuario();
                var lista = bLusuario.ListarUsuario(new BEUsuario());

                var usuario = lista.Find(x => x.Correo == EmailDestino);

                if (usuario == null) throw new Exception("No se ha encontrado usuario con el correo ingresado, revise los datos.");

                string EmailOrigen = string.IsNullOrEmpty(ConfigurationManager.AppSettings["CorreoRemitente"]) ? throw new Exception("Parametro correo remitente no encontrado") : ConfigurationManager.AppSettings["CorreoRemitente"];
                string Contrasena = string.IsNullOrEmpty(ConfigurationManager.AppSettings["ClaveRemitente"]) ? throw new Exception("Parametro clave remitente no encontrado") : ConfigurationManager.AppSettings["ClaveRemitente"];
                string smtpserver = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPServer"]) ? throw new Exception("Parametro servidor correo no encontrado") : ConfigurationManager.AppSettings["SMTPServer"];
                string smtpport = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPPort"]) ? throw new Exception("Parametro puerto servidor correo no encontrado") : ConfigurationManager.AppSettings["SMTPPort"];
                string enablessl = string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableSSL"]) ? "false" : ConfigurationManager.AppSettings["EnableSSL"];
            
                var cuerpohtml = ConstantesRecursos.CorreoCuerpoRecuperacion;

                cuerpohtml = cuerpohtml.Replace("[USER]", usuario.Nombre);
                cuerpohtml = cuerpohtml.Replace("[PASSWORD]", usuario.Clave);

                MailMessage oMailMessage = new MailMessage(EmailOrigen, EmailDestino, "Recuperación de Contraseña", cuerpohtml);
                oMailMessage.IsBodyHtml = true;

                SmtpClient oSmtpClient = new SmtpClient(smtpserver);
                oSmtpClient.EnableSsl = Convert.ToBoolean(enablessl);
                oSmtpClient.UseDefaultCredentials = false;

                oSmtpClient.Port = Convert.ToInt32(smtpport);
                oSmtpClient.Credentials = new System.Net.NetworkCredential(EmailOrigen, Contrasena);

                oSmtpClient.Send(oMailMessage);
                oSmtpClient.Dispose();

                var result = HideEmail(EmailDestino);

                var sb = new StringBuilder();
                sb.AppendLine("Sus credenciales fueron enviadas a su correo, ");
                sb.AppendLine(result + ".");
                sb.AppendLine("Por favor revisar su bandeja de correo.");

                emailCript = sb.ToString();
            }
            catch (Exception ex)
            {
                emailCript = ex.Message;
            }

            return emailCript;
        }

        public string HideEmail(string email)
        {
            //string email = "ejemplo123@ejemplo.com";
            string[] separada = email.Split('@');
            int inicio = 1; //Caracteres al inicio de la cadena que dejamos visibles
            int final = 3; //Caracteres al final de la cadena que dejamos visibles
            int longitud;
            if (separada[0].Length > inicio + final)
                longitud = separada[0].Length - final - inicio;
            else
                longitud = 1;

            separada[0] = separada[0].Remove(inicio, longitud).Insert(inicio, new string('*', longitud));
            email = String.Join("@", separada);

            return email;

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AceptaTC()
        {
          
            try
            {
                var usuario = (BEUsuario)Session["sistema.usuario"];
                BLusuario usbl = new BLusuario();
                usbl.UsuarioAceptaTC(usuario.Id_Usuario,usuario.Id_Usuario);

                usuario.AceptoTc = 1;
                Session.Add("sistema.usuario", usuario);

                return RedirectToAction("Index", "Home");
            }
            catch 
            {
                Session.Abandon();
                Session.Clear();
                Session.RemoveAll();
                System.Web.Security.FormsAuthentication.SignOut();
                return RedirectToAction("Login", "Seguridad");

            }
        }
    }
}