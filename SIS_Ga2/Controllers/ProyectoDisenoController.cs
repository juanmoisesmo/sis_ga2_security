﻿using SIS_Ga2.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SIS_Ga2.Controllers
{
    public class ProyectoDisenoController : Controller
    {
        // GET: ProyectoDiseno

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult DatosProyecto()
        {

            List<SelectListItem> lstReglamento = new List<SelectListItem>
            {
                new SelectListItem() { Text = "Manual de Carreteras MTC", Value = "1" },
                new SelectListItem() { Text = "Pavimentos Urbanos CE. 010", Value = "2" }
            };
            ViewData["ddlReglamento"] = lstReglamento;


            BLDepartamento objblDepartamento = new BLDepartamento();
            List<SelectListItem> data_list = new List<SelectListItem>();
            data_list.AddRange(objblDepartamento.ListarDepartamentos(0).Select(a => new SelectListItem() { Text = a.Departamento.ToUpper(), Value = Convert.ToString(a.Id_Departamento) }));
            ViewData["ddlDepartamento"] = data_list;
            ViewData["ddlProvincia"] = new SelectList(new[] { "(Selecciona)" });
            ViewData["ddlDistrito"] = new SelectList(new[] { "(Selecciona)" });

            return PartialView("_DatosProyecto");
        }

    }
}