﻿using SIS_Ga2.Business;
using SIS_Ga2.Entity;
using SIS_Ga2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;

namespace SIS_Ga2.Controllers
{
    public class ContactoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        public string SendEmail(ContactoModel contacto)
        {
            try
            {

                string EmailOrigen = string.IsNullOrEmpty(ConfigurationManager.AppSettings["CorreoRemitente"]) ? throw new Exception("Parametro correo remitente no encontrado") : ConfigurationManager.AppSettings["CorreoRemitente"];
                string EmailDestino = string.IsNullOrEmpty(ConfigurationManager.AppSettings["CorreoDestino"]) ? throw new Exception("Parametro correo remitente no encontrado") : ConfigurationManager.AppSettings["CorreoDestino"];
                string Contrasena = string.IsNullOrEmpty(ConfigurationManager.AppSettings["ClaveRemitente"]) ? throw new Exception("Parametro clave remitente no encontrado") : ConfigurationManager.AppSettings["ClaveRemitente"];
                string smtpserver = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPServer"]) ? throw new Exception("Parametro servidor correo no encontrado") : ConfigurationManager.AppSettings["SMTPServer"];
                string smtpport = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPPort"]) ? throw new Exception("Parametro puerto servidor correo no encontrado") : ConfigurationManager.AppSettings["SMTPPort"];
                string enablessl = string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableSSL"]) ? "false" : ConfigurationManager.AppSettings["EnableSSL"];

                var cuerpohtml = ConstantesRecursos.CorreoContacto;

                cuerpohtml = cuerpohtml.Replace("[Nombre]", contacto.Nombre);
                cuerpohtml = cuerpohtml.Replace("[Mensaje]", contacto.Mensaje);
                cuerpohtml = cuerpohtml.Replace("[Correo]", contacto.Correo);
                cuerpohtml = cuerpohtml.Replace("[Telefono]", contacto.Telefono);

                var asunto = string.IsNullOrEmpty(contacto.Asunto) ? "CONSULTAS FORMULARIO CONTACTO" : contacto.Asunto;

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;


                MailMessage oMailMessage = new MailMessage(EmailOrigen, EmailDestino,asunto, cuerpohtml);
                oMailMessage.IsBodyHtml = true;

                SmtpClient oSmtpClient = new SmtpClient(smtpserver);

                oSmtpClient.Port = Convert.ToInt32(smtpport);
                oSmtpClient.EnableSsl = Convert.ToBoolean(enablessl);
                oSmtpClient.UseDefaultCredentials = false;
                oSmtpClient.Credentials = new System.Net.NetworkCredential(EmailOrigen, Contrasena);
                oSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                oSmtpClient.Send(oMailMessage);
                oSmtpClient.Dispose();

                return "Se he enviado correctamente el correo de contacto";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            
        }

    }
}