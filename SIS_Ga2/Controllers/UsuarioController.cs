﻿using SIS_Ga2.Business;
using SIS_Ga2.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SIS_Ga2.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Index()
        {
            var lstestado = new List<SelectListItem>
            {
                new SelectListItem { Text = "TODOS", Value = "" },
                new SelectListItem { Text = "ACTIVO", Value = "1" },
                new SelectListItem { Text = "INACTIVO", Value = "0" }
            };
            ViewData["ddlEstado"] = lstestado;

            return View();
        }

        public ActionResult MantenimientoNuevo()
        {
            Session["usuario.mant"] = new BEUsuario();
            return View("Mantenimiento");
        }

        public ActionResult MantenimientoEditar(int idusuario)
        {
            var bLusuario = new BLusuario();
            var lista = bLusuario.ListarUsuario(new BEUsuario());

            var usuario = lista.Find(x => x.Id_Usuario == idusuario);

            Session["usuario.mant"] = usuario;

            return View("Mantenimiento");
        }

        public JsonResult ListarUsuario(BEUsuario usuario)
        {
            var bLusuario = new BLusuario();
            var lista = bLusuario.ListarUsuario(usuario);

            return Json(new { data = lista }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GrabarDatos(BEUsuario usuario)
        {
            try
            {
                var bLusuario = new BLusuario();
                bLusuario.GrabarDatos(usuario);

                return Json(new { data = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { data = "ERROR" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ObtenerDatos(int idusuario)
        {
            var bLusuario = new BLusuario();
            var lista = bLusuario.ListarUsuario(new BEUsuario());

            var usuario = lista.Find(x => x.Id_Usuario == idusuario);

            return Json(new { data = usuario }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult EliminarUsuario(int IdUsuarioSesion, int IdUsuarioDelete, string estado)
        {
            try
            {
                var iestado = estado == "0" ? 1 : 0;

                var bLusuario = new BLusuario();
                bLusuario.EliminarUsuario(new BEUsuario { Id_Usuario = IdUsuarioDelete, Usr_Actualizacion = IdUsuarioSesion.ToString(), Estado = iestado });

                return Json(new { data = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { data = "ERROR" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}