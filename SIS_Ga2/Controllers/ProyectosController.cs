﻿using Newtonsoft.Json;
using SIS_Ga2.Business;
using SIS_Ga2.Entity;
using SIS_Ga2.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SIS_Ga2.Controllers
{
    public class ProyectosController : Controller
    {
        // GET: Proyectos
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarProyectos(string NumProyecto, string FechaProyecto, string FechaContrato, string IdUsuario, string Distrito, string Provincia, string Departamento, string idTipoDiseno)
        {
            Departamento = Departamento == "0" ? "" : Departamento;
            Provincia = Provincia == "0" ? "" : Provincia;
            Distrito = Distrito == "0" ? "" : Distrito;

            var idtipodiseno = idTipoDiseno == "0" ? (int?)null : Int32.Parse(idTipoDiseno);


            var blproyectodiseno = new BLProyectoDiseno();
            var lista = blproyectodiseno.ListarTodoProyectoDiseno(IdUsuario, idtipodiseno);

            var listaresult = new List<BEProyecto>();


            lista.ForEach(x =>
            {
                var modelostr = x.Json_Data;
                dynamic modelo = null;

                if (x.Tipo_Diseno == 1)
                    modelo = JsonConvert.DeserializeObject<DisenoAsfaltoModel>(modelostr);

                if (x.Tipo_Diseno == 2)
                    modelo = JsonConvert.DeserializeObject<DisenoAdoquinModel>(modelostr);

                if (x.Tipo_Diseno == 3)
                    modelo = JsonConvert.DeserializeObject<DisenoConcretoModel>(modelostr);

                listaresult.Add(new BEProyecto
                {
                    Id_Diseno = (int)x.Id_Diseno,
                    Id_Proyecto = 0,
                    Id_TipoDiseno = x.Tipo_Diseno,
                    Num_Proyecto = modelo.datos_proyecto.numero_proyecto,
                    Proyecto = modelo.datos_proyecto.proyecto,
                    Fecha_Proyecto_Date = modelo.datos_proyecto.fecha_proyecto,
                    Ingeniero = modelo.datos_proyecto.ingeniero,
                    Num_Diseno = modelo.datos_proyecto.diseno,
                    Tipo_Diseno = modelo.datos_proyecto.tipo_diseno.valor_descripcion,
                    Tramo = modelo.datos_proyecto.tramo,
                    Departamento = modelo.datos_proyecto.departamento.valor_selector,
                    Provincia = modelo.datos_proyecto.provincia.valor_selector,
                    Distrito = modelo.datos_proyecto.distrito.valor_selector,
                    Ubigeo = string.Format("{0}/{1}/{2}", modelo.datos_proyecto.departamento.valor_descripcion, modelo.datos_proyecto.provincia.valor_descripcion, modelo.datos_proyecto.distrito.valor_descripcion),
                });
            });


            if (!string.IsNullOrEmpty(NumProyecto))
                listaresult = listaresult.FindAll(x => x.Num_Proyecto == NumProyecto);

            if (!string.IsNullOrEmpty(FechaProyecto))
                listaresult = listaresult.FindAll(x => x.Fecha_Proyecto_Date == FechaProyecto);

            if (!string.IsNullOrEmpty(FechaContrato))
                listaresult = listaresult.FindAll(x => x.Fecha_Contrato_Date == FechaContrato);

            if (!string.IsNullOrEmpty(Departamento))
                listaresult = listaresult.FindAll(x => x.Departamento == Departamento);

            if (!string.IsNullOrEmpty(Provincia))
                listaresult = listaresult.FindAll(x => x.Provincia == Provincia);

            if (!string.IsNullOrEmpty(Distrito))
                listaresult = listaresult.FindAll(x => x.Distrito == Distrito);


            return Json(new { data = listaresult }, JsonRequestBehavior.AllowGet);

        }
    }
}