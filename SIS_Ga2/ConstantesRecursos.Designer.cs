﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIS_Ga2 {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ConstantesRecursos {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ConstantesRecursos() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SIS_Ga2.ConstantesRecursos", typeof(ConstantesRecursos).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;p&gt;
        /// Hola, buen dia.
        ///&lt;/p&gt;
        ///&lt;p&gt;
        ///  Soy [Nombre], y tengo las siguientes consultas.
        ///&lt;/p&gt;
        ///&lt;p&gt;
        ///  [Mensaje]
        ///&lt;/p&gt;
        ///&lt;p&gt;
        /// De antemano gracias por su tiempo, mis datos de contacto son:
        ///&lt;/p&gt;
        ///&lt;p&gt;
        /// Correo Electrónico: [Correo]
        ///&lt;/p&gt;
        ///&lt;p&gt;
        /// Número Telefónico: [Telefono]
        ///&lt;/p&gt;
        ///&lt;p&gt;
        ///Saludos
        ///&lt;/p&gt;.
        /// </summary>
        internal static string CorreoContacto {
            get {
                return ResourceManager.GetString("CorreoContacto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;p&gt;
        ///Estimado usuario, [USER].
        ///&lt;/p&gt;
        ///&lt;p&gt;
        ///  Se remite el correo con su contraseña, de acuerdo a su solicitud de recuperación.
        ///&lt;/p&gt;
        ///&lt;p&gt;
        ///  Contraseña: [PASSWORD]
        ///&lt;/p&gt;
        ///&lt;p&gt;
        ///Saludos
        ///&lt;/p&gt;.
        /// </summary>
        internal static string CorreoCuerpoRecuperacion {
            get {
                return ResourceManager.GetString("CorreoCuerpoRecuperacion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table width=&quot;90%&quot; style=&quot;border-collapse: collapse&quot;&gt;
        ///  &lt;thead style=&quot;color:white;background-color:green;&quot;&gt;
        ///    &lt;tr&gt;
        ///      &lt;th colspan=&quot;4&quot; style=&quot;text-align:center;&quot;&gt;REPORTE DISEÑO DE PAVIMENTO&lt;/th&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;th colspan=&quot;4&quot; style=&quot;text-align:center;&quot;&gt;Método AASHTO 1993&lt;/th&gt;
        ///    &lt;/tr&gt;
        ///  &lt;/thead&gt;
        ///  &lt;tbody&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;Proyecto:&lt;/td&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;[Proyec [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string TablaDisenoReporteAsfalto {
            get {
                return ResourceManager.GetString("TablaDisenoReporteAsfalto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table width=&quot;90%&quot; style=&quot;border-collapse: collapse&quot;&gt;
        ///  &lt;thead style=&quot;color:white;background-color:green;&quot;&gt;
        ///    &lt;tr&gt;
        ///      &lt;th colspan=&quot;4&quot; style=&quot;text-align:center;&quot;&gt;REPORTE DISEÑO DE PAVIMENTO&lt;/th&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;th colspan=&quot;4&quot; style=&quot;text-align:center;&quot;&gt;Método AASHTO 1993&lt;/th&gt;
        ///    &lt;/tr&gt;
        ///  &lt;/thead&gt;
        ///  &lt;tbody&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;Proyecto:&lt;/td&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;[Proyec [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string TablaDisenoReporteConcreto {
            get {
                return ResourceManager.GetString("TablaDisenoReporteConcreto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table width=&quot;99%&quot; style=&quot;border: 1px solid black;border-collapse: collapse;font-weight:bold;text-align:center;&quot;&gt;
        ///  &lt;thead&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;4&quot; style=&quot;text-align:center;border: 1px solid black;background-color:green;color:white;&quot;&gt;
        ///        CUADRO COMPARATIVO - DISEÑO DE PAVIMENTOS
        ///      &lt;/td&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;td  colspan=&quot;4&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///     &lt;tr&gt;
        ///      &lt;td  colspan=&quot;4&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///  &lt;/thead&gt;
        ///  &lt;tbody&gt;
        ///    &lt;tr&gt;
        ///      &lt;th&gt;&lt;/th&gt;
        ///   [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string TableComparativoDisenoAdoCon {
            get {
                return ResourceManager.GetString("TableComparativoDisenoAdoCon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table width=&quot;99%&quot; style=&quot;border: 1px solid black;border-collapse: collapse;font-weight:bold;text-align:center;&quot;&gt;
        ///  &lt;thead&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;4&quot; style=&quot;text-align:center;border: 1px solid black;background-color:green;color:white;&quot;&gt;
        ///        CUADRO COMPARATIVO - DISEÑO DE PAVIMENTOS
        ///      &lt;/td&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;td  colspan=&quot;4&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///      &lt;tr&gt;
        ///      &lt;td  colspan=&quot;4&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///  &lt;/thead&gt;
        ///  &lt;tbody&gt;
        ///    &lt;tr&gt;
        ///      &lt;th&gt;&lt;/th&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string TableComparativoDisenoAsfAdo {
            get {
                return ResourceManager.GetString("TableComparativoDisenoAsfAdo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table width=&quot;99%&quot; style=&quot;border: 1px solid black;border-collapse: collapse;font-weight:bold;text-align:center;&quot;&gt;
        ///  &lt;thead&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;4&quot; style=&quot;text-align:center;border: 1px solid black;background-color:green;color:white;&quot;&gt;
        ///        CUADRO COMPARATIVO - DISEÑO DE PAVIMENTOS
        ///      &lt;/td&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;td  colspan=&quot;4&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///        &lt;tr&gt;
        ///      &lt;td  colspan=&quot;4&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///  &lt;/thead&gt;
        ///  &lt;tbody&gt;
        ///    &lt;tr&gt;
        ///      &lt;th&gt;&lt;/th&gt;        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string TableComparativoDisenoAsfCon {
            get {
                return ResourceManager.GetString("TableComparativoDisenoAsfCon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table width=&quot;99%&quot; style=&quot;border: 1px solid black;border-collapse: collapse;font-weight:bold;text-align:center;&quot;&gt;
        ///  &lt;thead&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;4&quot; style=&quot;text-align:center;border: 1px solid black;background-color:green;color:white;&quot;&gt;
        ///        CUADRO COMPARATIVO - DISEÑO DE PAVIMENTOS
        ///      &lt;/td&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;td  colspan=&quot;4&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///        &lt;tr&gt;
        ///      &lt;td  colspan=&quot;4&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///  &lt;/thead&gt;
        ///  &lt;tbody&gt;
        ///    &lt;tr&gt;
        ///      &lt;th&gt;&lt;/th&gt;        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string TableComparativoDisenoTodos {
            get {
                return ResourceManager.GetString("TableComparativoDisenoTodos", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table width=&quot;90%&quot; style=&quot;border-collapse: collapse&quot;&gt;
        ///  &lt;thead style=&quot;color:white;background-color:green;&quot;&gt;
        ///    &lt;tr&gt;
        ///      &lt;th colspan=&quot;4&quot; style=&quot;text-align:center;&quot;&gt;REPORTE DISEÑO DE PAVIMENTO&lt;/th&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;th colspan=&quot;4&quot; style=&quot;text-align:center;&quot;&gt;Método AASHTO 1993&lt;/th&gt;
        ///    &lt;/tr&gt;
        ///  &lt;/thead&gt;
        ///  &lt;tbody&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;&amp;nbsp;&amp;nbsp;&lt;/td&gt;
        ///    &lt;/tr&gt;
        ///    &lt;tr&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;Proyecto:&lt;/td&gt;
        ///      &lt;td colspan=&quot;2&quot;&gt;[Proyec [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string TableDisenoReporteAdoquin {
            get {
                return ResourceManager.GetString("TableDisenoReporteAdoquin", resourceCulture);
            }
        }
    }
}
